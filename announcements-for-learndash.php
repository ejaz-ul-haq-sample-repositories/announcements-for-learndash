<?php
/**
 * Plugin Name: Announcements For LearnDash
 * Plugin URI: http://www.wpinnovators.com
 * Description: This add-on will allow you to design beautiful announcements and notify your students in the browser.
 * Version: 1.0.0
 * Requires at least: 5.1
 * Requires PHP: 7.4
 * Author: WPinnovators
 * Author URI: http://www.wpinnovators.com
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wi-af-ld
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WP_List_Table class inclusion
 */
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	require_once( ABSPATH . 'wp-admin/includes/screen.php' );
	require_once( ABSPATH . 'wp-admin/includes/class-wp-screen.php' );
	require_once( ABSPATH . 'wp-admin/includes/template.php' );
}

/**
 * PSR-4 Composer Autoloader
 */
if ( file_exists( dirname( __FILE__ ) . '/includes/libraries/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/includes/libraries/autoload.php';
}

if ( ! function_exists( 'get_plugin_data' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

/**
 * App Bootstraping
 */
function init_wi_ld_announcements() {
	if ( \WI\AFLD\Bootstrap\Constants::get_instance()->register_constants()
	     && \WI\AFLD\Bootstrap\Requirements::get_instance()->validate_requirements()
	) {
		\WI\AFLD\Bootstrap\App::get_instance();
		do_action( 'wi_afld_loaded' );
	}
}

add_action( 'plugins_loaded', 'init_wi_ld_announcements' );