# Announcements For LearnDash

## Description

This add-on will allow you to design beautiful announcements and notify your students in the browser.

## Version 1.0.0

**Prerequisites**

- LearnDash Latest Version

**Features**
- Allow admin to create and manage beautiful interactive announcements.
- Allow admin to associate announcements with single or multiple groups or courses.
- Allow group leaders to create and manage beautiful integrative announcements for their administrative single or multiple groups or their courses.
- The student will see the announcement on the associated group or course page on the front end.
- The student will be able to search specific announcements or sort them.

## Installation

Before installation please make sure you have the latest LearnDash plugin installed.

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress.

## FAQ

### Can the group leader create announcements for the administrative groups or their associated courses?
RE: Yes, This addon allows the group leaders to create announcement like admin and associate it with the individual or multiple groups or groups' associated courses.

### Can the Group leader see the other group leader created announcements?
RE: No, The group leader will only see their own created announcements on the listing screen and the administrators' and other group leaders created announcements will be not listed due to privacy.


## Changelog

[See all version changelogs](CHANGELOG.md)

