<div class="wrap">
    <div id="wi-afld-group-announcements-wrapper">
        <table id="wi-afld-group-announcements-table" width="100%">
            <thead>
            <tr role="row">
                <th>Title</th>
                <th>Author</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>