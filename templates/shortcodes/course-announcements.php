<div class="wrap">
    <div id="wi-afld-course-announcements-wrapper">
        <table id="wi-afld-course-announcements-table" width="100%">
            <thead>
            <tr role="row">
                <th>Title</th>
                <th>Author</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>