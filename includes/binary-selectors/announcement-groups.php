<?php

namespace WI\AFLD\BinarySelectors;

if ( ( ! class_exists( '\WI\AFLD\BinarySelectors\Learndash_Binary_Selector_Announcement_Groups' ) ) && ( class_exists( 'Learndash_Binary_Selector_Posts' ) ) ) {

	class Learndash_Binary_Selector_Announcement_Groups extends \Learndash_Binary_Selector_Posts {

		public function __construct( $args = array() ) {
			$this->selector_class = get_class( $this );
			$defaults             = array(
				'announcement_id'    => 0,
				'post_type'          => 'groups',
				'html_title'         => '<h3>' . sprintf(
						esc_html_x( '%1$s Using %2$s', 'placeholders: Groups, Course', 'learndash' ),
						'Announcement',
						\LearnDash_Custom_Label::get_label( 'course' )
					) . '</h3>',
				'html_id'            => 'learndash_announcement_groups',
				'html_class'         => 'learndash_announcement_groups',
				'html_name'          => 'learndash_announcement_groups',
				'search_label_left'  => sprintf(
					esc_html_x( 'Search All %s', 'placeholder: Groups', 'learndash' ),
					\LearnDash_Custom_Label::get_label( 'groups' )
				),
				'search_label_right' => sprintf(
					esc_html_x( 'Search %1$s %2$s', 'placeholders: Course, Groups', 'learndash' ),
					'Announcement',
					\LearnDash_Custom_Label::get_label( 'groups' )
				),
			);
			$args                 = wp_parse_args( $args, $defaults );
			$args['html_id']      = $args['html_id'] . '-' . $args['announcement_id'];
			$args['html_name']    = $args['html_name'] . '[' . $args['announcement_id'] . ']';
			parent::__construct( $args );
		}
	}
}
