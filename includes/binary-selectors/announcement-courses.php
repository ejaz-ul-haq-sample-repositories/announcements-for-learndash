<?php

namespace WI\AFLD\BinarySelectors;

if ( ( ! class_exists( '\WI\AFLD\BinarySelectors\AnnouncementCourses' ) ) && ( class_exists( 'Learndash_Binary_Selector_Posts' ) ) ) {

	class AnnouncementCourses extends \Learndash_Binary_Selector_Posts {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;

		public function __construct( $args = array() ) {
			$this->selector_class = get_class( $this );
			$defaults             = array(
				'announcement_id'    => 0,
				'post_type'          => 'sfwd-courses',
				'html_title'         => '<h3>' . sprintf(
						esc_html_x( '%1$s %2$s', 'placeholders: Group, Courses', 'learndash' ),
						'Announcement',
						\LearnDash_Custom_Label::get_label( 'courses' )
					) . '</h3>',
				'html_id'            => 'learndash_announcement_courses',
				'html_class'         => 'learndash_announcement_courses',
				'html_name'          => 'learndash_announcement_courses',
				'search_label_left'  => sprintf(
					esc_html_x( 'Search All %1$s %2$s', 'placeholders: Group, Courses', 'learndash' ),
					'Announcement',
					\LearnDash_Custom_Label::get_label( 'courses' )
				),
				'search_label_right' => sprintf(
					esc_html_x( 'Search Assigned %1$s %2$s', 'placeholders: Group, Courses', 'learndash' ),
					'Announcement',
					\LearnDash_Custom_Label::get_label( 'courses' )
				),
			);
			$args                 = wp_parse_args( $args, $defaults );
			$args['html_id']      = $args['html_id'] . '-' . $args['announcement_id'];
			$args['html_name']    = $args['html_name'] . '[' . $args['announcement_id'] . ']';
			parent::__construct( $args );
		}
	}
}
