<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3b39ce65b07f869c10728992cb3fc7cb
{
    public static $prefixLengthsPsr4 = array (
        'W' => 
        array (
            'WI\\AFLD\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'WI\\AFLD\\' => 
        array (
            0 => __DIR__ . '/../../..' . '/includes',
        ),
    );

    public static $classMap = array (
        'ComposerAutoloaderInit3b39ce65b07f869c10728992cb3fc7cb' => __DIR__ . '/..' . '/composer/autoload_real.php',
        'Composer\\Autoload\\ClassLoader' => __DIR__ . '/..' . '/composer/ClassLoader.php',
        'Composer\\Autoload\\ComposerStaticInit3b39ce65b07f869c10728992cb3fc7cb' => __DIR__ . '/..' . '/composer/autoload_static.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'WI\\AFLD\\Admin\\Bootstrap' => __DIR__ . '/../../..' . '/includes/admin/bootstrap.php',
        'WI\\AFLD\\Admin\\RestAPI\\License' => __DIR__ . '/../../..' . '/includes/admin/rest-api/license.php',
        'WI\\AFLD\\Admin\\Settings\\License' => __DIR__ . '/../../..' . '/includes/admin/settings/license.php',
        'WI\\AFLD\\BinarySelectors\\AnnouncementCourses' => __DIR__ . '/../../..' . '/includes/binary-selectors/announcement-courses.php',
        'WI\\AFLD\\BinarySelectors\\Learndash_Binary_Selector_Announcement_Groups' => __DIR__ . '/../../..' . '/includes/binary-selectors/announcement-groups.php',
        'WI\\AFLD\\Bootstrap\\App' => __DIR__ . '/../../..' . '/includes/bootstrap/app.php',
        'WI\\AFLD\\Bootstrap\\Constants' => __DIR__ . '/../../..' . '/includes/bootstrap/constants.php',
        'WI\\AFLD\\Bootstrap\\Requirements' => __DIR__ . '/../../..' . '/includes/bootstrap/requirements.php',
        'WI\\AFLD\\CPT\\AnnouncementEdit' => __DIR__ . '/../../..' . '/includes/custom-post-types/class-learndash-admin-announcement-edit.php',
        'WI\\AFLD\\CPT\\Announcements' => __DIR__ . '/../../..' . '/includes/custom-post-types/announcements-cpt.php',
        'WI\\AFLD\\CPT\\Learndash_Admin_Announcements_Listing' => __DIR__ . '/../../..' . '/includes/custom-post-types/class-learndash-admin-announcements-listing.php',
        'WI\\AFLD\\License\\LicenseHandler' => __DIR__ . '/../../..' . '/includes/license/LicenseHandler.php',
        'WI\\AFLD\\License\\LicenseManager' => __DIR__ . '/../../..' . '/includes/license/LicenseManager.php',
        'WI\\AFLD\\License\\PluginUpdater' => __DIR__ . '/../../..' . '/includes/license/PluginUpdater.php',
        'WI\\AFLD\\Public\\Ajax\\Announcement' => __DIR__ . '/../../..' . '/includes/public/ajax/announcement.php',
        'WI\\AFLD\\Public\\Ajax\\CourseAnnouncements' => __DIR__ . '/../../..' . '/includes/public/ajax/course-announcements.php',
        'WI\\AFLD\\Public\\Ajax\\GroupAnnouncements' => __DIR__ . '/../../..' . '/includes/public/ajax/group-announcements.php',
        'WI\\AFLD\\Public\\App' => __DIR__ . '/../../..' . '/includes/public/app.php',
        'WI\\AFLD\\Public\\Classes\\CourseAnnouncements' => __DIR__ . '/../../..' . '/includes/public/classes/course-announcements.php',
        'WI\\AFLD\\Public\\Classes\\GroupAnnouncements' => __DIR__ . '/../../..' . '/includes/public/classes/group-announcements.php',
        'WI\\AFLD\\Public\\ShortCodes\\CourseAnnouncements' => __DIR__ . '/../../..' . '/includes/public/shortcodes/course-announcements.php',
        'WI\\AFLD\\Public\\ShortCodes\\GroupAnnouncements' => __DIR__ . '/../../..' . '/includes/public/shortcodes/group-announcements.php',
        'WI\\AFLD\\Settings\\Metaboxes\\LearnDash_Settings_Metabox_Announcement_Courses_Settings' => __DIR__ . '/../../..' . '/includes/settings/settings-metaboxes/class-ld-settings-metabox-announcement-courses.php',
        'WI\\AFLD\\Settings\\Metaboxes\\LearnDash_Settings_Metabox_Announcement_Groups_Settings' => __DIR__ . '/../../..' . '/includes/settings/settings-metaboxes/class-ld-settings-metabox-announcement-groups.php',
        'WI\\AFLD\\Settings\\Page\\AnnouncementsOptions' => __DIR__ . '/../../..' . '/includes/settings/settings-pages/class-ld-settings-page-courses-options.php',
        'WI\\AFLD\\Settings\\Page\\AnnouncementsShortcodes' => __DIR__ . '/../../..' . '/includes/settings/settings-pages/class-ld-settings-page-certificate-shortcodes.php',
        'WI\\AFLD\\Settings\\Sections\\AnnouncementCPT' => __DIR__ . '/../../..' . '/includes/settings/settings-sections/class-ld-settings-section-announcements-cpt.php',
        'WI\\AFLD\\Traits\\Course' => __DIR__ . '/../../..' . '/includes/traits/Course.php',
        'WI\\AFLD\\Traits\\Group' => __DIR__ . '/../../..' . '/includes/traits/Group.php',
        'WI\\AFLD\\Traits\\Helpers' => __DIR__ . '/../../..' . '/includes/traits/Helpers.php',
        'WI\\AFLD\\Traits\\RestAPI' => __DIR__ . '/../../..' . '/includes/traits/RestAPI.php',
        'WI\\AFLD\\Traits\\Singleton' => __DIR__ . '/../../..' . '/includes/traits/Singleton.php',
        'WI\\AFLD\\Traits\\Template' => __DIR__ . '/../../..' . '/includes/traits/Template.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3b39ce65b07f869c10728992cb3fc7cb::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3b39ce65b07f869c10728992cb3fc7cb::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit3b39ce65b07f869c10728992cb3fc7cb::$classMap;

        }, null, ClassLoader::class);
    }
}
