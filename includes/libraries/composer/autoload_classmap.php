<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname(dirname($vendorDir));

return array(
    'ComposerAutoloaderInit3b39ce65b07f869c10728992cb3fc7cb' => $vendorDir . '/composer/autoload_real.php',
    'Composer\\Autoload\\ClassLoader' => $vendorDir . '/composer/ClassLoader.php',
    'Composer\\Autoload\\ComposerStaticInit3b39ce65b07f869c10728992cb3fc7cb' => $vendorDir . '/composer/autoload_static.php',
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'WI\\AFLD\\Admin\\Bootstrap' => $baseDir . '/includes/admin/bootstrap.php',
    'WI\\AFLD\\Admin\\RestAPI\\License' => $baseDir . '/includes/admin/rest-api/license.php',
    'WI\\AFLD\\Admin\\Settings\\License' => $baseDir . '/includes/admin/settings/license.php',
    'WI\\AFLD\\BinarySelectors\\AnnouncementCourses' => $baseDir . '/includes/binary-selectors/announcement-courses.php',
    'WI\\AFLD\\BinarySelectors\\Learndash_Binary_Selector_Announcement_Groups' => $baseDir . '/includes/binary-selectors/announcement-groups.php',
    'WI\\AFLD\\Bootstrap\\App' => $baseDir . '/includes/bootstrap/app.php',
    'WI\\AFLD\\Bootstrap\\Constants' => $baseDir . '/includes/bootstrap/constants.php',
    'WI\\AFLD\\Bootstrap\\Requirements' => $baseDir . '/includes/bootstrap/requirements.php',
    'WI\\AFLD\\CPT\\AnnouncementEdit' => $baseDir . '/includes/custom-post-types/class-learndash-admin-announcement-edit.php',
    'WI\\AFLD\\CPT\\Announcements' => $baseDir . '/includes/custom-post-types/announcements-cpt.php',
    'WI\\AFLD\\CPT\\Learndash_Admin_Announcements_Listing' => $baseDir . '/includes/custom-post-types/class-learndash-admin-announcements-listing.php',
    'WI\\AFLD\\License\\LicenseHandler' => $baseDir . '/includes/license/LicenseHandler.php',
    'WI\\AFLD\\License\\LicenseManager' => $baseDir . '/includes/license/LicenseManager.php',
    'WI\\AFLD\\License\\PluginUpdater' => $baseDir . '/includes/license/PluginUpdater.php',
    'WI\\AFLD\\Public\\Ajax\\Announcement' => $baseDir . '/includes/public/ajax/announcement.php',
    'WI\\AFLD\\Public\\Ajax\\CourseAnnouncements' => $baseDir . '/includes/public/ajax/course-announcements.php',
    'WI\\AFLD\\Public\\Ajax\\GroupAnnouncements' => $baseDir . '/includes/public/ajax/group-announcements.php',
    'WI\\AFLD\\Public\\App' => $baseDir . '/includes/public/app.php',
    'WI\\AFLD\\Public\\Classes\\CourseAnnouncements' => $baseDir . '/includes/public/classes/course-announcements.php',
    'WI\\AFLD\\Public\\Classes\\GroupAnnouncements' => $baseDir . '/includes/public/classes/group-announcements.php',
    'WI\\AFLD\\Public\\ShortCodes\\CourseAnnouncements' => $baseDir . '/includes/public/shortcodes/course-announcements.php',
    'WI\\AFLD\\Public\\ShortCodes\\GroupAnnouncements' => $baseDir . '/includes/public/shortcodes/group-announcements.php',
    'WI\\AFLD\\Settings\\Metaboxes\\LearnDash_Settings_Metabox_Announcement_Courses_Settings' => $baseDir . '/includes/settings/settings-metaboxes/class-ld-settings-metabox-announcement-courses.php',
    'WI\\AFLD\\Settings\\Metaboxes\\LearnDash_Settings_Metabox_Announcement_Groups_Settings' => $baseDir . '/includes/settings/settings-metaboxes/class-ld-settings-metabox-announcement-groups.php',
    'WI\\AFLD\\Settings\\Page\\AnnouncementsOptions' => $baseDir . '/includes/settings/settings-pages/class-ld-settings-page-courses-options.php',
    'WI\\AFLD\\Settings\\Page\\AnnouncementsShortcodes' => $baseDir . '/includes/settings/settings-pages/class-ld-settings-page-certificate-shortcodes.php',
    'WI\\AFLD\\Settings\\Sections\\AnnouncementCPT' => $baseDir . '/includes/settings/settings-sections/class-ld-settings-section-announcements-cpt.php',
    'WI\\AFLD\\Traits\\Course' => $baseDir . '/includes/traits/Course.php',
    'WI\\AFLD\\Traits\\Group' => $baseDir . '/includes/traits/Group.php',
    'WI\\AFLD\\Traits\\Helpers' => $baseDir . '/includes/traits/Helpers.php',
    'WI\\AFLD\\Traits\\RestAPI' => $baseDir . '/includes/traits/RestAPI.php',
    'WI\\AFLD\\Traits\\Singleton' => $baseDir . '/includes/traits/Singleton.php',
    'WI\\AFLD\\Traits\\Template' => $baseDir . '/includes/traits/Template.php',
);
