<?php

namespace WI\AFLD\Bootstrap;

if (!class_exists('\WI\AFLD\Bootstrap\App')) {
    class App
    {

        /**
         * Traits used inside class
         */
        use \WI\AFLD\Traits\Helpers;
        use \WI\AFLD\Traits\Singleton;

        public function __construct()
        {
            add_filter('plugin_action_links_' . WIAFLD_MAIN_FILE_RELATIVE_PATH, array(
                $this,
                'register_plugin_actions_links'
            ), 10, 4);
            add_filter('plugin_row_meta', array($this, 'register_plugin_row_meta_links'), 10, 4);
            add_action('wi_afld_loaded', array($this, 'init_hooks'), 10);

            add_action('learndash_settings_pages_init', [$this, 'init_ld_custom_settings_pages'], 10);
            add_action('learndash_settings_sections_init', [$this, 'init_ld_custom_settings_sections'], 10);

            \WI\AFLD\CPT\AnnouncementEdit::get_instance();

            add_action('learndash_post_settings_metaboxes_init_' . $this->get_announcement_post_type(), [
                $this,
                'init_announcement_settings_metaboxes'
            ], 50, 1);

        }

        public function register_plugin_actions_links($actions, $plugin_file, $plugin_data, $context)
        {
            if (WIAFLD_MAIN_FILE_RELATIVE_PATH == $plugin_file) {
                $settings_url = add_query_arg(
                    array(
                        'page' => 'wi-afld',
                    ),
                    admin_url('admin.php')
                );
                $actions[] = '<a href="' . esc_url($settings_url) . '">' . __('Settings', WIAFLD_TEXT_DOMAIN) . '</a>';
            }

            return $actions;
        }

        public function register_plugin_row_meta_links($plugin_meta, $plugin_file, $plugin_data, $status)
        {
            if (WIAFLD_MAIN_FILE_RELATIVE_PATH === $plugin_file) {
                $plugin_meta[] = '<a href="https://www.wpinnovators.com/documentations/announcements-for-learndash/" target="_blank">' . __('Documentation', WIAFLD_TEXT_DOMAIN) . '</a>';
                $plugin_meta[] = '<a href="https://www.wpinnovators.com/open-support-ticket/" target="_blank">' . __('Support', WIAFLD_TEXT_DOMAIN) . '</a>';
            }

            return $plugin_meta;
        }

        public function init_hooks()
        {
            \WI\AFLD\CPT\Announcements::get_instance();
            \WI\AFLD\CPT\Learndash_Admin_Announcements_Listing::get_instance();
            \WI\AFLD\Admin\Bootstrap::get_instance();
            \WI\AFLD\Public\App::get_instance();
            add_action('init', array($this, 'setup_rest_api'), 10);
            add_action('init', array(
                \WI\AFLD\Admin\Settings\License::get_instance(),
                'register_custom_settings'
            ), 10);
        }

        public function setup_rest_api()
        {
            \WI\AFLD\Admin\RestAPI\License::get_instance();
        }

        public function init_ld_custom_settings_pages()
        {
            \WI\AFLD\Settings\Page\AnnouncementsOptions::add_page_instance();
            \WI\AFLD\Settings\Page\AnnouncementsShortcodes::add_page_instance();
        }

        public function init_ld_custom_settings_sections()
        {
            \WI\AFLD\Settings\Sections\AnnouncementCPT::add_section_instance();
        }

        public function init_announcement_settings_metaboxes($metaboxes)
        {
            if ((!isset($metaboxes['LearnDash_Settings_Metabox_Announcement_Courses_Settings'])) && (class_exists('\WI\AFLD\Settings\Metaboxes\LearnDash_Settings_Metabox_Announcement_Courses_Settings'))) {
                $metaboxes['LearnDash_Settings_Metabox_Announcement_Courses_Settings'] = \WI\AFLD\Settings\Metaboxes\LearnDash_Settings_Metabox_Announcement_Courses_Settings::add_metabox_instance();
            }
            if ((!isset($metaboxes['LearnDash_Settings_Metabox_Announcement_Groups_Settings'])) && (class_exists('\WI\AFLD\Settings\Metaboxes\LearnDash_Settings_Metabox_Announcement_Groups_Settings'))) {
                $metaboxes['LearnDash_Settings_Metabox_Announcement_Groups_Settings'] = \WI\AFLD\Settings\Metaboxes\LearnDash_Settings_Metabox_Announcement_Groups_Settings::add_metabox_instance();
            }

            return $metaboxes;
        }

    }
}