<?php

namespace WI\AFLD\Bootstrap;

if ( ! class_exists( '\WI\AFLD\Bootstrap\Requirements' ) ) {
	class Requirements {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;

		private $ld_plugin_data;
		private $ld_mc_plugin_data;

		public function __construct() {
			$this->ld_plugin_data    = get_plugin_data( WIAFLD_LD_MAIN_FILE_ABSOLUTE_PATH, false, false );
			$this->ld_mc_plugin_data = get_plugin_data( WIAFLD_MAIN_FILE_ABSOLUTE_PATH, false, false );
			add_action( 'wi_ld_mc_deactivate', array( $this, 'deactivate_ld_mc_plugin' ), 10 );
		}

		public function validate_requirements() {
			global $wp_version;
			$status = true;
			if ( file_exists( WIAFLD_LD_MAIN_FILE_ABSOLUTE_PATH ) ) {
				if ( ( ! is_plugin_active( WIAFLD_LD_MAIN_FILE_RELATIVE_PATH ) || ! class_exists( 'SFWD_LMS' ) ) ) {
					add_action( 'admin_notices', array( $this, 'ld_dependency_validation_admin_notice_error' ) );

					return false;
				} else {
					if ( version_compare( $this->ld_plugin_data['Version'], WIAFLD_LD_VERSION, '<' ) ) {
						add_action( 'admin_notices', array( $this, 'ld_dependency_validation_admin_notice_error' ) );

						return false;
					}
				}
			} else {
				add_action( 'admin_notices', array( $this, 'ld_dependency_validation_admin_notice_error' ) );

				return false;
			}

			return $status;
		}

		public function ld_dependency_validation_admin_notice_error() {
			do_action( 'wi_ld_mc_deactivate' );
			$class   = 'notice notice-error';
			$message = "";
			if ( file_exists( WIAFLD_LD_MAIN_FILE_ABSOLUTE_PATH ) ) {
				if ( ( ! is_plugin_active( WIAFLD_LD_MAIN_FILE_RELATIVE_PATH ) || ! class_exists( 'SFWD_LMS' ) ) ) {
					$message = __( 'Sorry ! The ' . $this->ld_mc_plugin_data['Name'] . ' is deactivated because the required ' . $this->ld_plugin_data['Name'] . ' Version ' . WIAFLD_LD_VERSION . ' is not found, Kindly install and activate and than try again. Thanks', WIAFLD_TEXT_DOMAIN );
				} else {
					if ( version_compare( $this->ld_plugin_data['Version'], '3.3.0.3', '<' ) ) {
						$message = __( 'Sorry ! The ' . $this->ld_mc_plugin_data['Name'] . ' is deactivated because the required ' . $this->ld_plugin_data['Name'] . ' Version ' . WIAFLD_LD_VERSION . ' is not found, Kindly install and activate and than try again. Thanks', WIAFLD_TEXT_DOMAIN );
					}
				}
			} else {
				$message = __( 'Sorry ! The ' . $this->ld_mc_plugin_data['Name'] . ' is deactivated because the required LearnDash Version > ' . WIAFLD_LD_VERSION . ' is not found,  Kindly install and activate and than try again. Thanks', WIAFLD_TEXT_DOMAIN );
			}
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
		}

		public function deactivate_ld_mc_plugin() {
			if ( is_plugin_active( WIAFLD_MAIN_FILE_RELATIVE_PATH ) || class_exists( '\WIAFLD\Bootstrap\App' ) ) {
				deactivate_plugins( WIAFLD_MAIN_FILE_RELATIVE_PATH, true );
			}
		}

	}
}