<?php

namespace WI\AFLD\Bootstrap;

if ( ! class_exists( '\WI\AFLD\Bootstrap\Constants' ) ) {
	class Constants {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Singleton;

		public function register_constants() {
			define( 'WIAFLD_PLUGIN_NAME', 'Announcements For LearnDash' );
			define( 'WIAFLD_VERSION', '1.0.0' );
			define( 'WIAFLD_TEXT_DOMAIN', 'wi-af-ld' );
			define( 'WIAFLD_LD_VERSION', '3.3.0' );
			$plugins                                     = get_plugins();
			$learndash_plugin_basename                   = '';
			$learndash_multi_certificate_plugin_basename = '';
			foreach ( $plugins as $plugin_main_file_path => $plugin ) {
				if ( $plugin['Name'] == 'LearnDash LMS' ) {
					$learndash_plugin_basename = $plugin_main_file_path;
				}
				if ( $plugin['Name'] == WIAFLD_PLUGIN_NAME ) {
					$learndash_multi_certificate_plugin_basename = $plugin_main_file_path;
				}
			}
			define( 'WIAFLD_LD_MAIN_FILE_RELATIVE_PATH', $learndash_plugin_basename );
			define( 'WIAFLD_LD_MAIN_FILE_ABSOLUTE_PATH', WP_PLUGIN_DIR . '/' . WIAFLD_LD_MAIN_FILE_RELATIVE_PATH );
			define( 'WIAFLD_MAIN_FILE_RELATIVE_PATH', $learndash_multi_certificate_plugin_basename );
			define( 'WIAFLD_MAIN_FILE_ABSOLUTE_PATH', WP_PLUGIN_DIR . '/' . WIAFLD_MAIN_FILE_RELATIVE_PATH );
			define( 'WIAFLD_BASE_DIR_', plugin_basename( WIAFLD_MAIN_FILE_ABSOLUTE_PATH ) );
			define( 'WIAFLD_DIR_PATH', plugin_dir_path( WIAFLD_MAIN_FILE_ABSOLUTE_PATH ) );
			define( 'WIAFLD_TEMPLATE_DIR_PATH', WIAFLD_DIR_PATH . 'templates' );
			define( 'WIAFLD_ASSETS_DIR_PATH', trailingslashit( WIAFLD_DIR_PATH . 'assets' ) );
			define( 'WIAFLD_BASE_URL', get_bloginfo( 'url' ) );
			define( 'WIAFLD_DIR_URL', trailingslashit( plugin_dir_url( WIAFLD_MAIN_FILE_ABSOLUTE_PATH ) ) );
			define( 'WIAFLD_ASSETS_URL', trailingslashit( WIAFLD_DIR_URL . 'assets' ) );

			return true;
		}
	}
}