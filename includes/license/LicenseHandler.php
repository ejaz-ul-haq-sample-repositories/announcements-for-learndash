<?php

namespace WI\AFLD\License;

/**
 * License handler for Learndash Attendance Add-on
 *
 * This class should simplify the process of adding license information
 * to new EDD extensions.
 *
 * @version 1.0
 */

if ( ! class_exists( '\WI\AFLD\License\LicenseHandler' ) ) {

	/**
	 * License_Handler Class
	 */
	class LicenseHandler {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;

		private $file;
		private $license_key;
		private $license_status;
		private $item_name;
		private $item_shortname;
		private $version;
		private $author;
		private $optname = '';

		private $active = false;
		private $deactive = false;
		private $expired = false;
		private $valid = false;
		private $invalid = false;
		private $failed = false;

		private $disabled = false;
		private $revoked = false;
		private $missing = false;
		private $site_inactive = false;
		private $item_name_mismatch = false;
		private $no_activations_left = false;

		private $api_url = 'https://www.wpinnovators.com';

		public $license_data;

		/**
		 * @var Plugin_Updater
		 */
		private $plugin_updater = null;

		/**
		 * Class constructor
		 *
		 * @param string $_file
		 * @param string $_item_name
		 * @param string $_version
		 * @param string $_author
		 * @param string $_optname
		 * @param string $_api_url
		 *
		 * @global  array $edd_options
		 */
		function __construct( $_file, $_item_name, $_version, $_author, $_optname = null, $_api_url = null ) {
			global $edd_options;

			$this->file           = $_file;
			$this->item_name      = $_item_name;
			$this->item_shortname = 'edd_' . preg_replace( '/[^a-zA-Z0-9_\s]/', '', str_replace( ' ', '_', strtolower( $this->item_name ) ) );
			$this->version        = $_version;
			$this->license_key    = get_option( 'wi_afld_license_key' );
			$this->author         = $_author;
			$this->optname        = $_optname;
			$this->license_status = get_option( 'wi_afld_license_status' );
			$this->api_url        = is_null( $_api_url ) ? $this->api_url : $_api_url;

			// check license on page load for updating status to display admin notices
			add_action( 'admin_init', array( $this, 'check_license' ), 10 );
		}

		public function get_license_key() {
			return $this->license_key;
		}

		public function get_license_status() {
			return $this->license_status;
		}

		/**
		 * Activate the license key
		 *
		 * @access  public
		 * @return  void
		 */
		public function activate_license( $license ) {
			// Data to send to the API
			$api_params = array(
				'edd_action' => 'activate_license',
				'license'    => $license,
				'item_name'  => urlencode( $this->item_name ),
				'url'        => urlencode( home_url() ),
				'time'       => current_time( 'timestamp' ),
			);
			// Call the API
			$response = wp_remote_get(
				esc_url_raw( add_query_arg( $api_params, $this->api_url ) ),
				array(
					'timeout'   => 15,
					'body'      => $api_params,
					'sslverify' => false,
				)
			);
			// Make sure there are no errors
			if ( is_wp_error( $response ) ) {
				return false;
			} else {
				// Decode license data
				$this->license_data = json_decode( wp_remote_retrieve_body( $response ) );
				$this->invalid      = ( isset( $this->license_data->license ) && 'invalid' == $this->license_data->license ) ? true : false;
				$this->active       = ( isset( $this->license_data->license ) && 'valid' == $this->license_data->license ) ? true : false;
				$this->expired      = ( isset( $this->license_data->error ) && 'expired' == $this->license_data->error ) ? true : false;
				update_option( 'wi_afld_license_status', $this->license_data->license );

				return true;
			}
		}


		/**
		 * Deactivate the license key
		 *
		 * @access  public
		 * @return  void
		 */
		public function deactivate_license( $license ) {
			// Data to send to the API
			$api_params = array(
				'edd_action' => 'deactivate_license',
				'license'    => $license,
				'item_name'  => urlencode( $this->item_name ),
				'url'        => urlencode( home_url() ),
				'time'       => current_time( 'timestamp' ),
			);
			// Call the API
			$response = wp_remote_get(
				esc_url_raw( add_query_arg( $api_params, $this->api_url ) ),
				array(
					'timeout'   => 15,
					'body'      => $api_params,
					'sslverify' => false,
				)
			);
			// Make sure there are no errors
			if ( is_wp_error( $response ) ) {
				return false;
			} else {
				// Decode the license data
				$this->license_data = json_decode( wp_remote_retrieve_body( $response ) );
				$this->invalid      = isset( $this->license_data->error ) ? $this->license_data->error : false;
				if ( $this->license_data->license === 'deactivated' ) {
					$this->deactive = true;
				} elseif ( $this->license_data->license === 'failed' ) {
					$this->failed = true;
				}
				update_option( 'wi_afld_license_status', $this->license_data->license );

				return true;
			}
		}

		/**
		 * Check License info
		 *
		 * @access  public
		 * @return  void
		 */
		public function check_license() {
			$check_license_data = get_transient( 'wi_afld_license_data' );
			if ( ! empty( $check_license_data ) && is_object( $check_license_data ) ) {
				$this->license_data = $check_license_data;
			} else {

				// Data to send to the API
				$api_params = array(
					'edd_action' => 'check_license',
					'license'    => $this->license_key,
					'item_name'  => urlencode( $this->item_name ),
					'url'        => urlencode( home_url() ),
					'time'       => current_time( 'timestamp' ),
				);
				// Call the API
				$response = wp_remote_get(
					esc_url_raw( add_query_arg( $api_params, $this->api_url ) ),
					array(
						'timeout'   => 15,
						'body'      => $api_params,
						'sslverify' => false,
					)
				);
				// Make sure there are no errors
				if ( is_wp_error( $response ) ) {
					return false;
				} else {
					// Decode the license data
					$license_data       = json_decode( wp_remote_retrieve_body( $response ) );
					$this->license_data = $license_data;
					set_transient( 'wi_afld_license_data', $license_data, DAY_IN_SECONDS );
					update_option( 'wi_afld_license_status', $this->license_data->license );
				}
			}
			if ( empty( $this->license_data ) || ! is_object( $this->license_data ) ) {
				return false;
			} else {
				switch ( $this->license_data->license ) {
					case 'valid':
						$this->valid = true;
						break;
					case 'invalid':
						$this->invalid = true;
						break;
					case 'site_inactive':
						$this->invalid = true;
						break;
					default:
						$this->valid = true;
						break;
				}
				if ( isset( $this->license_data->error ) ) {
					switch ( $this->license_data->error ) {
						case 'expired':
							$this->expired = true;
							break;
						case 'disabled':
							$this->disabled = true;
							break;
						case 'revoked':
							$this->revoked = true;
							break;
						case 'missing':
							$this->missing = true;
							break;
						case 'site_inactive':
							$this->site_inactive = true;
							break;
						case 'item_name_mismatch':
							$this->item_name_mismatch = true;
							break;
						case 'no_activations_left':
							$this->no_activations_left = true;
							break;
						default:
							$this->failed = true;
							break;
					}
				}
			}
		}

		public function is_valid() {
			return $this->valid;
		}

		/**
		 * Check if license is active
		 *
		 * @return bool
		 */
		public function is_active() {
			return $this->active;
		}


		public function is_deactive() {
			return $this->deactive;
		}

		public function is_failed() {
			return $this->failed;
		}

		/**
		 * Check if license is expired
		 *
		 * @return bool
		 */
		public function is_expired() {
			return $this->expired;
		}

		public function is_invalid() {
			return $this->invalid;
		}

		public function is_disabled() {
			return $this->disabled;
		}

		public function is_revoked() {
			return $this->revoked;
		}

		public function is_missing() {
			return $this->missing;
		}

		public function is_site_inactive() {
			return $this->site_inactive;
		}

		public function is_item_name_mismatch() {
			return $this->item_name_mismatch;
		}

		public function is_no_activations_left() {
			return $this->no_activations_left;
		}

		public function get_plugin_updater() {
			return $this->plugin_updater;
		}
	}
}