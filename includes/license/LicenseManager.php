<?php

namespace WI\AFLD\License;

if ( ! class_exists( '\WI\LD\License\LicenseManager' ) ) {
	class LicenseManager {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;

		public $license_handler = null;
		public $plugin_updater = null;
		public $api_url = 'https://www.wpinnovators.com';
		public $plugin_data;

		public function __construct() {

			$this->plugin_data = get_plugin_data( WIAFLD_MAIN_FILE_ABSOLUTE_PATH );
			add_action( 'admin_notices', array( $this, 'show_license_expire_or_invalid' ), 20 );

			$this->license_handler = new \WI\AFLD\License\LicenseHandler(
				WIAFLD_MAIN_FILE_ABSOLUTE_PATH,
				WIAFLD_PLUGIN_NAME,
				$this->plugin_data['Version'],
				$this->plugin_data['AuthorName']
			);

			$this->plugin_updater = new \WI\AFLD\License\PluginUpdater(
				$this->api_url,
				WIAFLD_MAIN_FILE_ABSOLUTE_PATH,
				array(
					'version'   => $this->plugin_data['Version'],
					'license'   => get_option( 'wi_afld_license_key' ),
					'item_name' => WIAFLD_PLUGIN_NAME,
					'author'    => $this->plugin_data['AuthorName'],
				)
			);

		}

		public function get_license_admin_notice_data() {
			if ( ! isset( $this->license_handler ) ) {
				return false;
			} else {
				$license_setting_url = add_query_arg(
					array(
						'page' => 'wi-afld',
					),
					admin_url( 'admin.php' )
				);
				$admin_notice_type   = '';
				$admin_notice_msg    = '';

				if ( $this->license_handler->is_active() ) {
					$admin_notice_type = 'success';
					$admin_notice_msg  = __( 'License Activated!', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_deactive() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'License Deactivated !', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_expired() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'Sorry! your license is expired, please enter a valid license key for ' . $this->plugin_data['Name'] . ' Addon to receive latest updates.', WIAFLD_TEXT_DOMAIN );
					//    $admin_notice_msg = sprintf(
					//        __(
					//            'Your license key for <strong>' . $this->plugin_data['Name'] . ' Addon</strong> has been expired on %s.
					//        You will not receive any future updates for this addon.
					//        Please purchase the addon from our site, <a href="#">here</a>
					//        to receive a valid license key.',
					//            WIAFLD_TEXT_DOMAIN
					//        ),
					//        date_i18n( get_option( 'date_format' ), strtotime( $this->license_handler->license_data->expires, current_time( 'timestamp' ) ) )
					//    );
				} elseif ( $this->license_handler->is_disabled() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'Your license key has been disabled.', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_revoked() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'Your license key has been disabled / revoked.', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_missing() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'Sorry ! we did not found provided license key, your license is Invalid.', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_site_inactive() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'Your license is not active for this URL.', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_item_name_mismatch() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'This appears to be an invalid license key for %s.', WIAFLD_TEXT_DOMAIN, $this->plugin_data['Name'] );
				} elseif ( $this->license_handler->is_no_activations_left() ) {
					$admin_notice_type = 'error';
					$admin_notice_msg  = __( 'Your license key has reached its activation limit.', WIAFLD_TEXT_DOMAIN );
				} elseif ( $this->license_handler->is_invalid() ) {
					$admin_notice_type = 'error';
					// $admin_notice_msg  = esc_html_x( 'Please enter a valid license key for <b>' . $this->plugin_data['Name'] . '</b> Addon to receive the latest updates.', WIAFLD_TEXT_DOMAIN );
					// $admin_notice_msg  = sprintf(
					// 	_x( 'Please enter a valid license key for %1$s Addon to receive the latest updates. Please click %2$s and update your license.', 'placeholders: Plugin name. Plugin license update link.', WIAFLD_TEXT_DOMAIN ),
					// 	'<strong>' . $this->plugin_data['Name'] . '</strong>',
					// 	'<a href="' . get_admin_url( null, 'admin.php?page=wi-afld' ) . '">' . esc_html__( 'here', WIAFLD_TEXT_DOMAIN ) . '</a>'
					// );
					$admin_notice_msg  = sprintf(
						_x( 'Please enter a valid license key for %1$s Addon to receive the latest updates.', 'placeholders: Plugin name.', WIAFLD_TEXT_DOMAIN ),
						 $this->plugin_data['Name'],
					);
				}

				$admin_notice_data = [
					'admin_notice_type'    => $admin_notice_type,
					'admin_notice_message' => $admin_notice_msg
				];

				return $admin_notice_data;
			}
		}

		public function show_license_expire_or_invalid() {
			if ( ! isset( $this->license_handler ) ) {
				return false;
			} else {
				$admin_notice_data = $this->get_license_admin_notice_data();
				if ( empty( $this->license_handler->get_license_key() )
				     || 'valid' != $this->license_handler->get_license_status()
				) {
					if ( 'success' == $admin_notice_data['admin_notice_type'] ) { ?>
                        <div class="notice notice-success is-dismissible">
                            <p><?php echo $admin_notice_data['admin_notice_message']; ?></p>
                        </div>
						<?php
					} elseif ( 'error' == $admin_notice_data['admin_notice_type'] ) {
						?>
                        <div class="error notice wi-afld-admin-notice is-dismissible">
                            <p><?php echo $admin_notice_data['admin_notice_message']; ?></p>
                        </div>
						<?php
					}
				}
			}
		}

		/**
		 * @return License_Handler
		 */
		public function get_license_handler() {
			return $this->license_handler;
		}

		/**
		 * @return Plugin_Updater
		 */
		public function get_plugin_updater() {
			return $this->plugin_updater;
		}


	}
}