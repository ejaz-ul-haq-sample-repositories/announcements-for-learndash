<?php

namespace WI\AFLD\CPT;

require_once LEARNDASH_LMS_PLUGIN_DIR . 'includes/admin/class-learndash-admin-posts-listing.php';

if ( ( class_exists( 'Learndash_Admin_Posts_Listing' ) ) && ( ! class_exists( '\WI\AFLD\CPT\Learndash_Admin_Announcements_Listing' ) ) ) {

	/**
	 * Class LearnDash Announcements Posts Listing.
	 */
	class Learndash_Admin_Announcements_Listing extends \Learndash_Admin_Posts_Listing {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Singleton;
		use \WI\AFLD\Traits\Helpers;

		public function __construct() {
			$this->post_type = $this->get_announcement_post_type();

			parent::__construct();
		}

		/**
		 * Called via the WordPress init action hook.
		 */
		public function listing_init() {
			if ( $this->listing_init_done ) {
				return;
			}

			parent::listing_init();

			$this->listing_init_done = true;
		}

		/**
		 * Call via the WordPress load sequence for admin pages.
		 */
		public function on_load_listing() {
			if ( $this->post_type_check() ) {
				parent::on_load_listing();

				add_filter( 'learndash_listing_table_query_vars_filter', array(
					$this,
					'listing_table_query_vars_filter_groups'
				), 30, 3 );

			}
		}

		/**
		 * Listing table query vars
		 */
		public function listing_table_query_vars_filter_groups( $q_vars, $post_type, $query ) {
			if ( $post_type === $this->post_type ) {
				if ( ! learndash_is_admin_user() ) {
					if ( ( learndash_is_group_leader_user( get_current_user_id() ) ) ) {
						$q_vars['author'] = get_current_user_id();
					}
				}
			}

			return $q_vars;
		}

		// End of functions.
	}
}

