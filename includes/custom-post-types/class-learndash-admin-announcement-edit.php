<?php

namespace WI\AFLD\CPT;

require_once LEARNDASH_LMS_PLUGIN_DIR . 'includes/admin/class-learndash-admin-posts-edit.php';

use Learndash_Admin_Post_Edit;

if ( ( class_exists( '\Learndash_Admin_Post_Edit' ) ) && ( ! class_exists( '\WI\AFLD\CPT\AnnouncementEdit' ) ) ) {

	class AnnouncementEdit extends \Learndash_Admin_Post_Edit {

		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;

		private $use_course_builder = false;
		private $course_builder = null;

		public function __construct() {
			$this->post_type = $this->get_announcement_post_type();
			parent::__construct();
		}

		public function on_load() {
			if ( $this->post_type_check() ) {
				parent::on_load();
				/** This filter is documented in includes/admin/class-learndash-admin-posts-edit.php */
				$this->_metaboxes = apply_filters( 'learndash_post_settings_metaboxes_init_' . $this->post_type, $this->_metaboxes );
			}
		}

		public function add_metaboxes( $post_type = '', $post = null ) {
			if ( $this->post_type_check( $post_type ) ) {
				parent::add_metaboxes( $post_type, $post );
			}
		}

		public function save_post( $post_id = 0, $post = null, $update = false ) {
			if ( ! $this->post_type_check( $post ) ) {
				return false;
			}
			if ( ! parent::save_post( $post_id, $post, $update ) ) {
				return false;
			}
			if ( ! empty( $this->_metaboxes ) ) {
				foreach ( $this->_metaboxes as $_metaboxes_instance ) {
					$settings_fields = array();
					$settings_fields = $_metaboxes_instance->get_post_settings_field_updates( $post_id, $post, $update );
					$_metaboxes_instance->save_post_meta_box( $post_id, $post, $update, $settings_fields );
				}
			}
		}
		
	}
}

