<?php

namespace WI\AFLD\CPT;

if ( ! class_exists( '\WI\AFLD\CPT\Announcements' ) ) {
	class Announcements {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Singleton;
		use \WI\AFLD\Traits\Helpers;

		public $post_type_slug;

		public function __construct() {
			$this->post_type_slug = $this->get_announcement_post_type();
			add_action( 'learndash_admin_tabs_set', [ $this, 'update_learndash_admin_tabs_set' ], 10, 2 );
			add_filter( 'learndash_header_tab_menu', [ $this, 'update_learndash_header_tab_menu' ], 60, 4 );
			add_filter( 'learndash_header_data', [ $this, 'update_learndash_header_data' ], 10, 3 );
			add_filter( 'learndash_submenu', [ $this, 'add_announcement_menu' ], 10, 1 );
			add_filter( 'learndash_post_args', [ $this, 'register_announcement_cpt' ], 10, 1 );
		}

		public function update_learndash_header_tab_menu( $header_data_tabs, $menu_tab_key, $screen_post_type ) {
			global $pagenow, $post, $typenow;
			// Reorder tabs Content, Builder, Settings, Anything else.
			if ( ( $this->get_announcement_post_type() == $screen_post_type ) && ( ! empty( $header_data_tabs ) ) && ( in_array( $pagenow, array(
					'post.php',
					'post-new.php'
				), true ) ) ) {
				$header_data_tabs[1]['id']                  = $this->get_announcement_post_type() . '-groups';
				$header_data_tabs[1]['name']                = 'Groups';
				$header_data_tabs[1]['showDocumentSidebar'] = false;
				$header_data_tabs[1]['metaboxes'][]         = 'learndash_announcement_groups';
				$header_data_tabs[2]['id']                  = $this->get_announcement_post_type() . '-courses';
				$header_data_tabs[2]['name']                = 'Courses';
				$header_data_tabs[2]['showDocumentSidebar'] = false;
				$header_data_tabs[2]['metaboxes'][]         = 'learndash_announcement_courses';
			}

			return $header_data_tabs;
		}

		public function update_learndash_header_data( $header_data, $menu_tab_key, $admin_tab_sets ) {
			return $header_data;
		}

		public function update_learndash_admin_tabs_set( $current_screen_parent_file, $Learndash_Admin_Menus_Tabs ) {
			if ( 'edit.php?post_type=' . $this->get_announcement_post_type() == $current_screen_parent_file ) {
				global $submenu;
				$Learndash_Admin_Menus_Tabs->add_admin_tab_set( 'edit.php?post_type=' . $this->get_announcement_post_type(), $submenu[ 'edit.php?post_type=' . $this->get_announcement_post_type() ] );
				$Learndash_Admin_Menus_Tabs->add_admin_tab_item(
					$current_screen_parent_file,
					array(
						'id'   => 'edit-' . $this->get_announcement_post_type(),
						'name' => esc_html_x( 'Announcements', 'Announcements Menu Label', 'learndash' ),
						'link' => 'edit.php?post_type=' . $this->get_announcement_post_type(),
						'cap'  => 'edit_announcements',
					),
					$Learndash_Admin_Menus_Tabs->admin_tab_priorities['high']
				);
			}

			return $current_screen_parent_file;
		}

		public function add_announcement_menu( $add_submenu ) {
			global $submenu;
			if ( current_user_can( 'edit_announcements' ) ) {
				if ( isset( $submenu[ 'edit.php?post_type=' . $this->get_announcement_post_type() ] ) ) {
					$add_submenu[ $this->get_announcement_post_type() ] = array(
						'name'  => 'Announcements',
						'cap'   => 'edit_announcements',
						'link'  => 'edit.php?post_type=' . $this->get_announcement_post_type(),
						'class' => 'submenu-ldlms-announcements',
					);
				}
			}
			$menu_slug = 'edit.php?post_type=' . $this->post_type_slug;
			remove_menu_page( $menu_slug );

			return $add_submenu;
		}

		public function register_announcement_cpt( $post_args ) {
			$lcl_announcement                                    = 'Announcement';
			$lcl_announcements                                   = 'Announcements';
			$announcement_labels                                 = array(
				'name'                     => $lcl_announcements,
				'singular_name'            => $lcl_announcement,
				'add_new'                  => esc_html_x( 'Add New', 'placeholder: announcement', 'learndash' ),
				// translators: placeholder: announcement.
				'add_new_item'             => sprintf( esc_html_x( 'Add New %s', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcement.
				'edit_item'                => sprintf( esc_html_x( 'Edit %s', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcement.
				'new_item'                 => sprintf( esc_html_x( 'New %s', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				'all_items'                => $lcl_announcements,
				// translators: placeholder: announcement.
				'view_item'                => sprintf( esc_html_x( 'View %s', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcements.
				'view_items'               => sprintf( esc_html_x( 'View %s', 'placeholder: announcements', 'learndash' ), $lcl_announcements ),
				// translators: placeholder: announcements.
				'search_items'             => sprintf( esc_html_x( 'Search %s', 'placeholder: announcements', 'learndash' ), $lcl_announcements ),
				// translators: placeholder: announcements.
				'not_found'                => sprintf( esc_html_x( 'No %s found', 'placeholder: announcements', 'learndash' ), $lcl_announcements ),
				// translators: placeholder: announcements.
				'not_found_in_trash'       => sprintf( esc_html_x( 'No %s found in Trash', 'placeholder: announcements', 'learndash' ), $lcl_announcements ),
				'parent_item_colon'        => '',
				'menu_name'                => $lcl_announcements,
				// translators: placeholder: announcement.
				'item_published'           => sprintf( esc_html_x( '%s Published', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcement.
				'item_published_privately' => sprintf( esc_html_x( '%s Published Privately', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcement.
				'item_reverted_to_draft'   => sprintf( esc_html_x( '%s Reverted to Draft', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcement.
				'item_scheduled'           => sprintf( esc_html_x( '%s Scheduled', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				// translators: placeholder: announcement.
				'item_updated'             => sprintf( esc_html_x( '%s Updated', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
			);
			$announcement_taxonomies                             = array();
			$announcement_taxonomies['category']                 = 'category';
			$announcement_taxonomies['post_tag']                 = 'post_tag';
			$learndash_settings_permalinks_taxonomies            = array();
			$learndash_settings_permalinks_taxonomies            = wp_parse_args(
				$learndash_settings_permalinks_taxonomies,
				array(
					'ld_announcement_category' => 'announcement-category',
					'ld_announcement_tag'      => 'announcement-tag',
					'ld_lesson_category'       => 'lesson-category',
					'ld_lesson_tag'            => 'lesson-tag',
					'ld_topic_category'        => 'topic-category',
					'ld_topic_tag'             => 'topic-tag',
					'ld_quiz_category'         => 'quiz-category',
					'ld_quiz_tag'              => 'quiz-tag',
					'ld_question_category'     => 'question-category',
					'ld_question_tag'          => 'question-tag',
					'ld_group_category'        => 'group-category',
					'ld_group_tag'             => 'group-tag',
				)
			);
			$announcement_taxonomies['ld_announcement_category'] = array(
				'public'            => true,
				'hierarchical'      => true,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'show_in_rest'      => true,
				'rewrite'           => array( 'slug' => 'ld_announcement_category' ),
				'capabilities'      => array(
					'manage_terms' => 'manage_categories',
					'edit_terms'   => 'edit_categories',
					'delete_terms' => 'delete_categories',
					'assign_terms' => 'assign_categories',
				),
				'labels'            => array(
					// translators: placeholder: announcement.
					'name'              => sprintf( esc_html_x( '%s Categories', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'singular_name'     => sprintf( esc_html_x( '%s Category', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'search_items'      => sprintf( esc_html_x( 'Search %s Categories', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'all_items'         => sprintf( esc_html_x( 'All %s Categories', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'parent_item'       => sprintf( esc_html_x( 'Parent %s Category', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'parent_item_colon' => sprintf( esc_html_x( 'Parent %s Category:', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'edit_item'         => sprintf( esc_html_x( 'Edit %s Category', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'update_item'       => sprintf( esc_html_x( 'Update %s Category', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'add_new_item'      => sprintf( esc_html_x( 'Add New %s Category', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'new_item_name'     => sprintf( esc_html_x( 'New %s Category Name', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'menu_name'         => sprintf( esc_html_x( '%s Categories', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				),
			);
			$announcement_taxonomies['ld_announcement_tag']      = array(
				'public'            => true,
				'hierarchical'      => false,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'show_in_rest'      => true,
				'rewrite'           => array( 'slug' => 'ld_announcement_tag' ),
				'labels'            => array(
					// translators: placeholder: announcement.
					'name'              => sprintf( esc_html_x( '%s Tags', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'singular_name'     => sprintf( esc_html_x( '%s Tag', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'search_items'      => sprintf( esc_html_x( 'Search %s Tag', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'all_items'         => sprintf( esc_html_x( 'All %s Tags', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'parent_item'       => sprintf( esc_html_x( 'Parent %s Tag', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'parent_item_colon' => sprintf( esc_html_x( 'Parent %s Tag:', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'edit_item'         => sprintf( esc_html_x( 'Edit %s Tag', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'update_item'       => sprintf( esc_html_x( 'Update %s Tag', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'add_new_item'      => sprintf( esc_html_x( 'Add New %s Tag', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'new_item_name'     => sprintf( esc_html_x( 'New %s Tag Name', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
					// translators: placeholder: announcement.
					'menu_name'         => sprintf( esc_html_x( '%s Tags', 'placeholder: announcement', 'learndash' ), $lcl_announcement ),
				),
			);
			$this->learndash_init_admin_announcements_capabilities();
			$announcement_capabilities                        = $this->learndash_get_admin_announcements_capabilities();
			$post_args[ $this->get_announcement_post_type() ] = array(
				'plugin_name'       => 'announcement',
				'slug_name'         => 'announcements',
				'post_type'         => $this->get_announcement_post_type(),
				'template_redirect' => true,
				'taxonomies'        => $announcement_taxonomies,
				'cpt_options'       => array(
					'has_archive'         => learndash_post_type_has_archive( $this->get_announcement_post_type() ),
					'hierarchical'        => false,
					'supports'            => array(
						'title',
						'editor',
						'author',
						'page-attributes',
						'thumbnail',
						'revisions'
					),
					'labels'              => $announcement_labels,
					'capability_type'     => 'announcement',
					'exclude_from_search' => false,
					'capabilities'        => $announcement_capabilities,
					'map_meta_cap'        => true,
					'show_in_rest'        => true,
				),
			);

			return $post_args;
		}

		function learndash_get_admin_announcements_capabilities(): array {
			return array(
				'read_post'              => 'read_announcement',
				'publish_posts'          => 'publish_announcements',
				'edit_posts'             => 'edit_announcements',
				'edit_others_posts'      => 'edit_others_announcements',
				'delete_posts'           => 'delete_announcements',
				'delete_others_posts'    => 'delete_others_announcements',
				'read_private_posts'     => 'read_private_announcements',
				'edit_private_posts'     => 'edit_private_announcements',
				'delete_private_posts'   => 'delete_private_announcements',
				'delete_post'            => 'delete_announcement',
				'edit_published_posts'   => 'edit_published_announcements',
				'delete_published_posts' => 'delete_published_announcements',
			);
		}

		function learndash_init_admin_announcements_capabilities(): void {
			$admin_role = get_role( 'administrator' );
			if ( is_null( $admin_role ) ) {
				return;
			}
			foreach ( $this->learndash_get_admin_announcements_capabilities() as $capability ) {
				if ( ! $admin_role->has_cap( $capability ) ) {
					$admin_role->add_cap( $capability );
				}
			}

			$group_leader = get_role( 'group_leader' );
			if ( is_null( $group_leader ) ) {
				return;
			}
			foreach ( $this->learndash_get_admin_announcements_capabilities() as $capability ) {
				if ( ! $group_leader->has_cap( $capability ) ) {
					$group_leader->add_cap( $capability );
				}
			}

		}

	}
}