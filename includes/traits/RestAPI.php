<?php

namespace WI\AFLD\Traits;

if ( ! trait_exists( '\WI\AFLD\Traits\RestAPI' ) ) {
	trait RestAPI {

		public $api_version = "v1";
		public $api_namespace = "wi-afld";

		public function get_api_base_url() {
			$api_base_url = $this->api_namespace . '/' . $this->api_version;

			return $api_base_url;
		}

		public function permissions() {
			return current_user_can( 'manage_options' );
		}

	}
}