<?php

namespace WI\AFLD\Traits;

if ( ! trait_exists( '\WI\AFLD\Traits\Helpers' ) ) {
	trait Helpers {

		public function get_announcement_post_type() {
			return 'ld-announcements';
		}

		public function dd( array $array ) {
			echo '<pre>';
			print_r( $array );
			echo '</pre>';
		}

		public function log( $log ) {
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( print_r( $log, true ) );
			} else {
				error_log( $log );
			}
		}

		public function get_date_time_format() {
			$date_time_format = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );

			return $date_time_format;
		}

		public function gmt_to_local( $date_time ) {
			// First we convert the timestamp to local Y-m-d H:i:s format
			$date_time_local = get_date_from_gmt( date( 'Y-m-d H:i:s', $date_time ), 'Y-m-d H:i:s' );
			// Then we take that value and reconvert it to a timestamp and call date_i18n to translate the month, date name etc.
			$date_time_local_formated = date_i18n( $this->get_date_time_format(), strtotime( $date_time_local ) );

			// $date_time_str       = strtotime( $date_time_local_formated );
			return $date_time_local_formated;
		}

		public function push_at_to_associative_array( $array, $key, $new ) {
			$keys  = array_keys( $array );
			$index = array_search( $key, $keys );
			$pos   = false === $index ? count( $array ) : $index + 1;
			$array = array_slice( $array, 0, $pos, true ) + $new + array_slice( $array, $pos, count( $array ) - 1, true );

			return $array;
		}

		public function is_user_course_enrolled( $user_id, $course_id ) {
			$course_enrolled_users = learndash_get_course_users_access_from_meta( $course_id );
			if ( is_array( $course_enrolled_users ) && count( $course_enrolled_users ) > 0 ) {
				if ( in_array( $user_id, $course_enrolled_users ) ) {
					return true;
				} else {
					return false;
				}
			}
		}

		public function get_quiz_completed_date( $quiz_id, $user_id ) {
			$quiz_completed_date = 0;
			$quiz_attempts_count = learndash_get_user_quiz_attempts_count( $user_id, $quiz_id );
			if ( $quiz_attempts_count > 0 ) {
				$quiz_attempts     = learndash_get_user_quiz_attempts( $user_id, $quiz_id );
				$quiz_last_attempt = ( ! empty( $quiz_attempts ) && $quiz_attempts_count > 1 ) ? end( $quiz_attempts ) : $quiz_attempts[0];
				if ( is_object( $quiz_last_attempt ) && isset( $quiz_last_attempt->activity_completed ) ) {
					$quiz_completed_date = $quiz_last_attempt->activity_completed;
				}
			}

			return $quiz_completed_date;
		}

	}
}