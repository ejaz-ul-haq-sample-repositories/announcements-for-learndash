<?php

namespace WI\AFLD\Traits;

if ( ! trait_exists( '\WI\AFLD\Traits\Course' ) ) {
	trait Course {

		function afld_get_announcement_courses( $announcement_id = 0, $bypass_transient = false ) {
			$course_ids      = array();
			$announcement_id = absint( $announcement_id );
			if ( ! empty( $announcement_id ) ) {
				$query_args = array(
					'post_type'      => learndash_get_post_type_slug( 'course' ),
					'fields'         => 'ids',
					'posts_per_page' => - 1,
					'meta_query'     => array(
						array(
							'key'     => $this->get_course_announcement_meta_key( $announcement_id ),
							'compare' => 'EXISTS',
						),
					),
				);
				$query      = new \WP_Query( $query_args );
				if ( ( is_a( $query, 'WP_Query' ) ) && ( property_exists( $query, 'posts' ) ) ) {
					$course_ids = $query->posts;
				}
			}

			return $course_ids;
		}

		function afld_get_course_announcements( $course_id = 0, $bypass_transient = false ) {
			$announcements_ids = array();
			$course_id         = absint( $course_id );
			if ( ! empty( $course_id ) ) {
				$course_post_meta = get_post_meta( $course_id );
				if ( ! empty( $course_post_meta ) ) {
					foreach ( $course_post_meta as $meta_key => $meta_set ) {
						if ( '_wi_afld_announcement_' == substr( $meta_key, 0, strlen( '_wi_afld_announcement_' ) ) ) {
							/**
							 * For Course Groups the meta_value is a datetime. This is the datetime the course
							 * was added to the group. So we need to pull the group_id from the meta_key.
							 */
							$announcement_id     = str_replace( '_wi_afld_announcement_', '', $meta_key );
							$announcements_ids[] = absint( $announcement_id );
						}
					}
				}
			}

			return $announcements_ids;
		}

		function afld_set_announcement_courses( $announcement_id = 0, $announcement_courses_new = array() ) {
			$announcement_id = absint( $announcement_id );
			if ( ! empty( $announcement_id ) ) {
				$announcement_courses_old       = $this->afld_get_announcement_courses( $announcement_id, true );
				$announcement_courses_intersect = array_intersect( $announcement_courses_new, $announcement_courses_old );
				$announcement_courses_add       = array_diff( $announcement_courses_new, $announcement_courses_intersect );
				if ( ! empty( $announcement_courses_add ) ) {
					foreach ( $announcement_courses_add as $course_id ) {
						$this->afld_update_announcement_course_association( $announcement_id, $course_id, false );
					}
				}
				$announcement_courses_remove = array_diff( $announcement_courses_old, $announcement_courses_intersect );
				if ( ! empty( $announcement_courses_remove ) ) {
					foreach ( $announcement_courses_remove as $course_id ) {
						$this->afld_update_announcement_course_association( $announcement_id, $course_id, true );
					}
				}
			}
		}

		public function get_announcement_course_meta_key( $course_id ) {
			return '_wi_afld_course_' . $course_id;
		}

		public function get_course_announcement_meta_key( $announcement_id ) {
			return '_wi_afld_announcement_' . $announcement_id;
		}

		function afld_update_announcement_course_association( $announcement_id = 0, $course_id = 0, $remove = false ) {
			$action_success  = false;
			$announcement_id = absint( $announcement_id );
			$course_id       = absint( $course_id );
			if ( ( ! empty( $announcement_id ) ) && ( ! empty( $course_id ) ) ) {
				$activity_type = 'group_access_course';
				if ( $remove ) {
					$action_success = true;
					delete_post_meta( $course_id, $this->get_course_announcement_meta_key( $announcement_id ) );
					do_action( 'wi_afld_removed_course_announcement_association', $course_id, $announcement_id );
				} else {
					$action_success = true;
					update_post_meta( $course_id, $this->get_course_announcement_meta_key( $announcement_id ), time() );
					do_action( 'wi_afld_added_course_announcement_association', $course_id, $announcement_id );
				}
			}

			return $action_success;
		}
		
	}
}