<?php

namespace WI\AFLD\Traits;

if ( ! trait_exists( '\WI\AFLD\Traits\Group' ) ) {
	trait Group {

		function afld_get_announcement_groups( $announcement_id = 0, $bypass_transient = false ) {
			$group_ids       = array();
			$announcement_id = absint( $announcement_id );
			if ( ! empty( $announcement_id ) ) {
				$query_args = array(
					'post_type'      => learndash_get_post_type_slug( 'group' ),
					'fields'         => 'ids',
					'posts_per_page' => - 1,
					'meta_query'     => array(
						array(
							'key'     => $this->get_group_announcement_meta_key( $announcement_id ),
							'compare' => 'EXISTS',
						),
					),
				);
				$query      = new \WP_Query( $query_args );
				if ( ( is_a( $query, 'WP_Query' ) ) && ( property_exists( $query, 'posts' ) ) ) {
					$group_ids = $query->posts;
				}
			}

			return $group_ids;
		}

		function afld_get_group_announcements( $group_id = 0, $bypass_transient = false ) {
			$announcements_ids = array();
			$group_id          = absint( $group_id );
			if ( ! empty( $group_id ) ) {
				$group_post_meta = get_post_meta( $group_id );
				if ( ! empty( $group_post_meta ) ) {
					foreach ( $group_post_meta as $meta_key => $meta_set ) {
						if ( '_wi_afld_announcement_' == substr( $meta_key, 0, strlen( '_wi_afld_announcement_' ) ) ) {
							/**
							 * For group Groups the meta_value is a datetime. This is the datetime the group
							 * was added to the group. So we need to pull the group_id from the meta_key.
							 */
							$announcement_id     = str_replace( '_wi_afld_announcement_', '', $meta_key );
							$announcements_ids[] = absint( $announcement_id );
						}
					}
				}
			}

			return $announcements_ids;
		}

		function afld_set_announcement_groups( $announcement_id = 0, $announcement_groups_new = array() ) {
			$announcement_id = absint( $announcement_id );
			if ( ! empty( $announcement_id ) ) {
				$announcement_groups_old       = $this->afld_get_announcement_groups( $announcement_id, true );
				$announcement_groups_intersect = array_intersect( $announcement_groups_new, $announcement_groups_old );
				$announcement_groups_add       = array_diff( $announcement_groups_new, $announcement_groups_intersect );
				if ( ! empty( $announcement_groups_add ) ) {
					foreach ( $announcement_groups_add as $group_id ) {
						$this->afld_update_announcement_group_association( $announcement_id, $group_id, false );
					}
				}
				$announcement_groups_remove = array_diff( $announcement_groups_old, $announcement_groups_intersect );
				if ( ! empty( $announcement_groups_remove ) ) {
					foreach ( $announcement_groups_remove as $group_id ) {
						$this->afld_update_announcement_group_association( $announcement_id, $group_id, true );
					}
				}

				/**
				 * Finally clear our cache for other services.
				 * $transient_key = 'learndash_group_groups_' . $group_id;
				 * LDLMS_Transients::delete( $transient_key );
				 */
			}
		}


		public function get_announcement_group_meta_key( $group_id ) {
			return '_wi_afld_group_' . $group_id;
		}

		public function get_group_announcement_meta_key( $announcement_id ) {
			return '_wi_afld_announcement_' . $announcement_id;
		}

		function afld_update_announcement_group_association( $announcement_id = 0, $group_id = 0, $remove = false ) {
			$action_success = false;

			$announcement_id = absint( $announcement_id );
			$group_id        = absint( $group_id );


			if ( ( ! empty( $announcement_id ) ) && ( ! empty( $group_id ) ) ) {
				if ( $remove ) {
					$action_success = true;
					delete_post_meta( $group_id, $this->get_group_announcement_meta_key( $announcement_id ) );
					do_action( 'wi_afld_removed_group_announcement_association', $group_id, $announcement_id );
				} else {
					$action_success = true;
					update_post_meta( $group_id, $this->get_group_announcement_meta_key( $announcement_id ), time() );
					do_action( 'wi_afld_added_group_announcement_association', $group_id, $announcement_id );
				}
			}

			return $action_success;
		}

	}
}