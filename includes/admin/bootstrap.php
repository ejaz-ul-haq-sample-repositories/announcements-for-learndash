<?php

namespace WI\AFLD\Admin;

if ( ! class_exists( '\WI\AFLD\Admin\Bootstrap' ) ) {
	class Bootstrap {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;

		private $parent_slug;
		private $page_title;
		private $menu_title;
		private $capability;
		private $menu_slug;
		private $function;

		public $page_menu;
		public $tab;

		public function __construct() {
			$this->parent_slug = 'learndash-lms';
			$this->page_title  = __( 'Announcements for LearnDash Settings', WIAFLD_TEXT_DOMAIN );
			$this->menu_title  = __( 'Announcements for LearnDash Settings', WIAFLD_TEXT_DOMAIN );
			$this->capability  = 'manage_options';
			$this->menu_slug   = 'wi-afld';
			$this->function    = array( $this, 'page' );
			add_action( 'admin_menu', array( $this, 'CreatePageMenu' ), 10 );
		}

		public function remove_all_admin_notices() {
			remove_all_actions( 'admin_notices' );
			remove_all_actions( 'all_admin_notices' );
			echo '<style>.notice{ display: none; }</style>';
		}

		public function CreatePageMenu() {
			$this->page_menu = add_submenu_page(
				$this->parent_slug,
				$this->page_title,
				$this->menu_title,
				$this->capability,
				$this->menu_slug,
				$this->function,
//				7
			);
			add_action( "admin_head-{$this->page_menu}", array( $this, 'remove_all_admin_notices' ), 10 );
			add_action( "admin_print_scripts-{$this->page_menu}", array( $this, 'dashboard_css_js' ), 10000 );
		}


		public function page() {
			echo '<div id="wi-afld"></div>';
		}

		public function dashboard_css_js() {
			$path       = WIAFLD_ASSETS_DIR_PATH . 'admin/build/admin.asset.php';
			$asset_file = include( $path );
			/*
			 * Add Extra JS Dependencies to array
			 * */
			$asset_file['dependencies'][] = 'jquery';
			$asset_file['dependencies'][] = 'wp-api';
			wp_register_script(
				'wi-afld-admin-script',
				WIAFLD_ASSETS_URL . 'admin/build/admin.js',
				$asset_file['dependencies'],
				$asset_file['version'],
				true
			);
			wp_enqueue_script( 'wi-afld-admin-script' );
			wp_register_style(
				'wi-afld-admin-style',
				WIAFLD_ASSETS_URL . 'admin/build/style-admin.css',
				array( 'wp-components' ),
				$asset_file['version'],
				'all'
			);
			wp_enqueue_style( 'wi-afld-admin-style' );
		}

	}
}