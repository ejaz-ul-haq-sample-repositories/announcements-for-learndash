<?php

namespace WI\AFLD\Admin\Settings;

if ( ! class_exists( '\WI\AFLD\Admin\Settings\License' ) ) {
	class License {
		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Singleton;
		use \WI\AFLD\Traits\Helpers;

		public function register_custom_settings() {
			register_setting(
				'wi_afld_settings',
				'wi_afld_text_domain',
				array(
					'type'         => 'string',
					'single'       => true,
					'show_in_rest' => true,
					'default'      => WIAFLD_TEXT_DOMAIN,
				)
			);
			register_setting(
				'wi_afld_settings',
				'wi_afld_license_key',
				array(
					'type'         => 'string',
					'single'       => true,
					'show_in_rest' => true,
					'default'      => '',
				)
			);
			register_setting(
				'wi_afld_settings',
				'wi_afld_license_status',
				array(
					'type'         => 'string',
					'single'       => true,
					'show_in_rest' => true,
					'default'      => '',
				)
			);

		}
	}
}