<?php

namespace WI\AFLD\Admin\RestAPI;

if ( ! class_exists( '\WI\AFLD\Admin\RestAPI\License' ) ) {
	class License {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Singleton;
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\RestAPI;

		public $License_Manager = '';
		public $license_handler = '';

		private static $api_route_base_url = '/license';

		public function __construct() {
			/**
			 * Authenticated User will be able to access
			 */
			add_action( 'rest_api_init', array( $this, 'add_secure_routes' ) );
			$this->License_Manager = new \WI\AFLD\License\LicenseManager();
			$this->license_handler = $this->License_Manager->get_license_handler();
			add_action( 'admin_head', array( $this, 'hide_dublicate_admin_notices' ), 10 );
		}

		public function hide_dublicate_admin_notices() {
			echo '<style>.wi-afld-admin-notice ~ .wi-afld-admin-notice { display: none; } </style>';
		}

		public function add_secure_routes() {
			register_rest_route(
				$this->get_api_base_url(),
				self::$api_route_base_url,
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'update_license_settings' ),
					'permission_callback' => array( $this, 'permissions' ),
				)
			);
		}

		public function permissions() {
			return current_user_can( 'manage_options' );
		}

		public function get_license_admin_notice( $license_key ) {
			if ( isset( $license_key ) && ! empty( $license_key ) ) {
				$admin_notice_data = $this->License_Manager->get_license_admin_notice_data();

				return $admin_notice_data;
			}
		}

		public function get_license_status( \WP_REST_Request $request ) {
			$request_arr = json_decode( $request->get_body() );
			if ( isset( $request_arr->license_key ) && ! empty( $request_arr->license_key ) ) {
				$admin_notice_data = $this->get_license_admin_notice( $request_arr->license_key );

				return $admin_notice_data;
			}
		}

		public function update_license_settings( \WP_REST_Request $request ) {
			$request_arr = json_decode( $request->get_body() );
			if ( isset( $request_arr->license_key ) && ! empty( $request_arr->license_key ) ) {
				$license_key_updated    = false;
				$license_status_updated = false;
				if ( $request_arr->license_edd_action == 'activate' ) {
					if ( $this->license_handler->activate_license( $request_arr->license_key ) ) {
						$license_status_updated = true;
					}
				} elseif ( $request_arr->license_edd_action == 'deactivate' ) {
					if ( $this->license_handler->deactivate_license( $request_arr->license_key ) ) {
						$license_status_updated = true;
					}
				}

				if ( get_option( 'wi_afld_license_key' ) != $request_arr->license_key ) {
					if ( update_option( 'wi_afld_license_key', $request_arr->license_key ) ) {
						$license_key_updated = true;
					}
				} else {
					$license_key_updated = true;
				}

				if ( $license_status_updated == true && $license_key_updated == true ) {
					$admin_notice_data = $this->get_license_admin_notice( $request_arr->license_key );

					return $admin_notice_data;
				}

			}
		}

	}
}