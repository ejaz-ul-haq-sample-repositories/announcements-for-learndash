<?php

namespace WI\AFLD\Settings\Metaboxes;

if ( ( class_exists( 'LearnDash_Settings_Metabox' ) ) && ( ! class_exists( '\WI\AFLD\Settings\Metaboxes\LearnDash_Settings_Metabox_Announcement_Groups_Settings' ) ) ) {

	class LearnDash_Settings_Metabox_Announcement_Groups_Settings extends \LearnDash_Settings_Metabox {

		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Group;

		public function __construct() {
			// What screen ID are we showing on.
			$this->settings_screen_id = $this->get_announcement_post_type();
			// Used within the Settings API to uniquely identify this section.
			$this->settings_metabox_key = 'learndash_announcement_groups';
			// Section label/header.
			$this->settings_section_label = sprintf(
				esc_html_x( '%1$s %2$s', 'placeholder: Announcement, Groups', 'learndash' ),
				'Announcement',
				learndash_get_custom_label( 'groups' )
			);
			parent::__construct();
		}

		protected function show_settings_metabox_fields( $metabox = null ) {
			if ( ( is_object( $metabox ) ) && ( is_a( $metabox, 'LearnDash_Settings_Metabox' ) ) && ( $metabox->settings_metabox_key === $this->settings_metabox_key ) ) {
				if ( ( isset( $metabox->post ) ) && ( is_a( $metabox->post, 'WP_Post ' ) ) ) {
					$announcement_id = $metabox->post->ID;
				} else {
					$announcement_id = get_the_ID();
				}
				if ( ( ! empty( $announcement_id ) ) && ( get_post_type( $announcement_id ) === $this->get_announcement_post_type() ) ) {
					$metabox_description = '';
					// Use nonce for verification.
					wp_nonce_field( 'learndash_announcement_groups_nonce_' . $announcement_id, 'learndash_announcement_groups_nonce' );
					if ( ! empty( $metabox_description ) ) {
						$metabox_description .= ' ';
					}
					?>
                    <div id="learndash_course_users_page_box" class="learndash_course_users_page_box">
						<?php
						if ( ! empty( $metabox_description ) ) {
							echo wp_kses_post( wpautop( $metabox_description ) );
						}
						$announcement_groups_args = array(
							'html_title'      => '',
							'announcement_id' => $announcement_id,
							'selected_ids'    => $this->afld_get_announcement_groups( $announcement_id ),
						);

						if ( ! learndash_is_admin_user() ) {
							if ( ( learndash_is_group_leader_user( get_current_user_id() ) ) ) {
								$group_ids = learndash_get_administrators_group_ids( get_current_user_id() );
								$group_ids = array_map( 'absint', $group_ids );
								if ( ! empty( $group_ids ) ) {
									$announcement_groups_args['included_ids'] = $group_ids;
								}
							}
						}

						$ld_binary_selector_course_groups = new \WI\AFLD\BinarySelectors\Learndash_Binary_Selector_Announcement_Groups( $announcement_groups_args );
						$ld_binary_selector_course_groups->show();
						?>
                    </div>
					<?php
				}
			}
		}

		public function save_post_meta_box( $post_id = 0, $saved_post = null, $update = null, $settings_field_updates = null ) {
			if ( true === $this->verify_metabox_nonce_field() ) {
				if ( ( isset( $_POST[ $this->settings_metabox_key . '-' . $post_id . '-changed' ] ) ) && ( ! empty( $_POST[ $this->settings_metabox_key . '-' . $post_id . '-changed' ] ) ) ) { // phpcs:ignore WordPress.Security.NonceVerification
					if ( ( isset( $_POST[ $this->settings_metabox_key ][ $post_id ] ) ) && ( ! empty( $_POST[ $this->settings_metabox_key ][ $post_id ] ) ) ) { // phpcs:ignore WordPress.Security.NonceVerification
						$announcement_groups = (array) json_decode( stripslashes( $_POST[ $this->settings_metabox_key ][ $post_id ] ) ); // phpcs:ignore WordPress.Security.NonceVerification, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
						$announcement_groups = array_map( 'absint', $announcement_groups );
						$this->afld_set_announcement_groups( $post_id, $announcement_groups );
					}
				}
			}
		}

	}
}
