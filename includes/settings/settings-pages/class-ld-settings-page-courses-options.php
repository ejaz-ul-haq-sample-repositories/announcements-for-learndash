<?php

namespace WI\AFLD\Settings\Page;

use LearnDash_Settings_Page;

if ( ( class_exists( 'LearnDash_Settings_Page' ) ) && ( ! class_exists( '\WI\AFLD\Settings\Page\AnnouncementsOptions' ) ) ) {

	class AnnouncementsOptions extends LearnDash_Settings_Page {

		use \WI\AFLD\Traits\Helpers;

		public function __construct() {
			$this->parent_menu_page_url = 'edit.php?post_type=' . $this->get_announcement_post_type();
			$this->menu_page_capability = LEARNDASH_ADMIN_CAPABILITY_CHECK;
			$this->settings_page_id     = 'announcements-options';
			$this->settings_page_title  = esc_html_x( 'Settings', 'Announcements Settings', 'learndash' );
			$this->settings_tab_title   = $this->settings_page_title;
			parent::__construct();
		}
	}
}

