<?php

namespace WI\AFLD\Public\Classes;

if ( ! class_exists( '\WI\AFLD\Public\Classes\GroupAnnouncements' ) ) {
	class GroupAnnouncements {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;

		public function __construct() {
			\WI\AFLD\Public\Ajax\GroupAnnouncements::get_instance();
			\WI\AFLD\Public\ShortCodes\GroupAnnouncements::get_instance();
			add_action( 'wp_enqueue_scripts', array( $this, 'group_announcements_css_js' ), 10 );
			add_filter( 'learndash_content_tabs', [ $this, 'init_group_announcements_tab' ], 10, 4 );
		}

		public function group_announcements_css_js() {
			global $post;
			if ( $post->post_type == learndash_get_post_type_slug( 'group' ) ) {
				wp_register_style( 'afld-group-announcements-tab-jquery-ui-style', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', [], time(), 'all' );
				wp_enqueue_style( 'afld-group-announcements-tab-jquery-ui-style' );
				wp_register_style( 'afld-group-announcements-tab-dataTables-jquery-ui-style', 'https://cdn.datatables.net/1.12.1/css/dataTables.jqueryui.min.css', [], time(), 'all' );
				wp_enqueue_style( 'afld-group-announcements-tab-dataTables-jquery-ui-style' );
				wp_register_style( 'afld-group-announcements-tab-style', WIAFLD_ASSETS_URL . 'public/css/group-announcements-tab.css', [], time(), 'all' );
				wp_enqueue_style( 'afld-group-announcements-tab-style' );
				/**
				 * JS
				 */
				wp_register_script( 'afld-group-announcements-tab-dataTables-script', 'https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js', array( 'jquery' ), '1.0.0', true );
				wp_enqueue_script( 'afld-group-announcements-tab-dataTables-script' );
				wp_register_script( 'afld-group-announcements-tab-dataTables-jqueryui-script', 'https://cdn.datatables.net/1.12.1/js/dataTables.jqueryui.min.js', array( 'jquery' ), '1.0.0', true );
				wp_enqueue_script( 'afld-group-announcements-tab-dataTables-jqueryui-script' );
			}
		}

		public function init_group_announcements_tab( $tabs, $context, $group_id, $user_id ) {
			if ( $context == 'group' && is_user_logged_in() && learndash_is_user_in_group( $user_id, $group_id ) ) {
				$tabs[] = array(
					'id'      => 'announcements',
					'icon'    => 'ld-icon-announcements dashicons dashicons-megaphone',
					'label'   => __( 'Announcements', 'learndash' ),
					'content' => do_shortcode( '[wi_afld_group_announcements]' ),
				);
			}

			return $tabs;
		}

	}
}