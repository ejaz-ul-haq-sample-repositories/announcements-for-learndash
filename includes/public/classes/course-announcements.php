<?php

namespace WI\AFLD\Public\Classes;

if ( ! class_exists( '\WI\AFLD\Public\Classes\CourseAnnouncements' ) ) {
	class CourseAnnouncements {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;

		public function __construct() {
			\WI\AFLD\Public\Ajax\CourseAnnouncements::get_instance();
			\WI\AFLD\Public\ShortCodes\CourseAnnouncements::get_instance();
			add_action( 'wp_enqueue_scripts', array( $this, 'course_announcements_css_js' ), 10 );
			add_filter( 'learndash_content_tabs', [ $this, 'init_course_announcements_tab' ], 10, 4 );
		}

		public function course_announcements_css_js() {
			global $post;
			if ( $post->post_type == learndash_get_post_type_slug( 'course' ) ) {
				wp_register_style( 'afld-course-announcements-tab-jquery-ui-style', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', [], time(), 'all' );
				wp_enqueue_style( 'afld-course-announcements-tab-jquery-ui-style' );
				wp_register_style( 'afld-course-announcements-tab-dataTables-jquery-ui-style', WIAFLD_ASSETS_URL . 'public/css/dataTables.jqueryui.min.css', [], time(), 'all' );
				wp_enqueue_style( 'afld-course-announcements-tab-dataTables-jquery-ui-style' );
				wp_register_style( 'afld-course-announcements-tab-style', WIAFLD_ASSETS_URL . 'public/css/course-announcements-tab.css', [], time(), 'all' );
				wp_enqueue_style( 'afld-course-announcements-tab-style' );
				/**
				 * JS
				 */
				wp_register_script( 'afld-course-announcements-tab-dataTables-script', WIAFLD_ASSETS_URL . 'public/js/jquery.dataTables.min.js', array( 'jquery' ), '1.0.0', true );
				wp_enqueue_script( 'afld-course-announcements-tab-dataTables-script' );
				wp_register_script( 'afld-course-announcements-tab-dataTables-jqueryui-script', WIAFLD_ASSETS_URL . 'public/js/dataTables.jqueryui.min.js', array( 'jquery' ), '1.0.0', true );
				wp_enqueue_script( 'afld-course-announcements-tab-dataTables-jqueryui-script' );
			}
		}

		public function init_course_announcements_tab( $tabs, $context, $course_id, $user_id ) {
			if ( $context == 'course' && is_user_logged_in() && ld_course_check_user_access( $course_id, $user_id ) ) {
				$tabs[] = array(
					'id'      => 'announcements',
					'icon'    => 'ld-icon-announcements dashicons dashicons-megaphone',
					'label'   => __( 'Announcements', 'learndash' ),
					'content' => do_shortcode( '[wi_afld_course_announcements]' ),
				);
			}

			return $tabs;
		}

	}
}