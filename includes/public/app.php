<?php

namespace WI\AFLD\Public;

if ( ! class_exists( '\WI\AFLD\Public\App' ) ) {
	class App {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;

		public function __construct() {
			add_filter( 'wp_robots', [ $this, 'ld_announcement_indexing' ], 10, 1 );
			\WI\AFLD\Public\Classes\GroupAnnouncements::get_instance();
			\WI\AFLD\Public\Classes\CourseAnnouncements::get_instance();
		}

		function ld_announcement_indexing( $robots ) {
			if ( is_singular( $this->get_announcement_post_type() ) ) {
				$robots['noindex']  = true;
				$robots['nofollow'] = true;
			}

			return $robots;
		}

	}
}