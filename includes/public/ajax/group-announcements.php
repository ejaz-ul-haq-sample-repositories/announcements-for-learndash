<?php

namespace WI\AFLD\Public\Ajax;

if ( ! class_exists( '\WI\AFLD\Public\Ajax\GroupAnnouncements' ) ) {
	class GroupAnnouncements {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;
		use \WI\AFLD\Traits\Group;

		public function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'group_announcements_ajax_js' ), 10 );
			add_action( 'wp_ajax_wi_afld_get_group_announcements', array(
				$this,
				'wi_afld_get_group_announcements_callback'
			), 10 );
			add_action( 'wp_ajax_nopriv_wi_afld_get_group_announcements', array(
				$this,
				'wi_afld_get_group_announcements_callback'
			), 10 );
			add_action( 'wp_ajax_wi_afld_get_announcement', array(
				$this,
				'wi_afld_get_announcement_callback'
			), 10 );
			add_action( 'wp_ajax_nopriv_wi_afld_get_announcement', array(
				$this,
				'wi_afld_get_announcement_callback'
			), 10 );
		}

		public function group_announcements_ajax_js() {
			global $post;
			if ( $post->post_type == learndash_get_post_type_slug( 'group' ) ) {
				wp_register_script( 'afld-group-announcements-tab-script', WIAFLD_ASSETS_URL . 'public/js/group-announcements-tab.js', array(
					'jquery',
					'afld-group-announcements-tab-dataTables-script',
					'afld-group-announcements-tab-dataTables-jqueryui-script'
				), '1.0.0', true );
				wp_localize_script( 'afld-group-announcements-tab-script', 'wi_afld_group_announcements', array(
					'utilities' => array(
						'user_id'  => ( is_user_logged_in() ) ? get_current_user_id() : 0,
						'group_id' => $post->ID
					),
					'ajax'      => array(
						'get'              => array(
							'url'    => esc_url_raw( admin_url( 'admin-ajax.php' ) ),
							'nonce'  => wp_create_nonce( 'ajax_wi_afld_get_group_announcements_nonce' ),
							'action' => 'wi_afld_get_group_announcements',
						),
						'get_announcement' => array(
							'url'    => esc_url_raw( admin_url( 'admin-ajax.php' ) ),
							'nonce'  => wp_create_nonce( 'ajax_wi_afld_get_announcement_nonce' ),
							'action' => 'wi_afld_get_announcement',
						),
					),
				) );
				wp_enqueue_script( 'afld-group-announcements-tab-script' );
			}
		}

		public function wi_afld_get_group_announcements_callback() {
			if ( check_ajax_referer( 'ajax_wi_afld_get_group_announcements_nonce', 'security' ) ) {
				$Result          = [];
				$Draw            = intval( $_GET['draw'] );
				$RecordsTotal    = 0;
				$RecordsFiltered = 0;
				$Records         = [];
				if ( count( $this->afld_get_group_announcements( intval( $_GET['group_id'] ) ) ) > 0 ) {
					$columns = array(
						0 => 'ID',
						1 => 'post_title',
						2 => 'post_author',
						3 => 'post_date'
					);
					$args    = array(
						'post_type'      => $this->get_announcement_post_type(),
						'post_status'    => 'publish',
						'posts_per_page' => $_GET['length'],
						'offset'         => $_GET['start'],
						'order'          => $_GET['order'][0]['dir'],
						'orderby'        => $columns[ $_GET['order'][0]['column'] ],
						'post__in'       => $this->afld_get_group_announcements( intval( $_GET['group_id'] ) ),
					);
					if ( ! empty( $_GET['search']['value'] ) ) {
						$args['s'] = $_GET['search']['value'];
					}
					$movie_query     = new \WP_Query( $args );
					$RecordsTotal    = $movie_query->found_posts;
					$RecordsFiltered = $movie_query->found_posts;
					if ( $movie_query->have_posts() ) {
						while ( $movie_query->have_posts() ) {
							$movie_query->the_post();
							$view_url  = add_query_arg( array(
								'action'          => 'wi_afld_get_announcement',
								'announcement_id' => get_the_ID(),
								'title'           => esc_url_raw( get_the_title() ),
								'source_id'       => intval( $_GET['group_id'] ),
								'height'          => intval( $_GET['height'] ),
								'width'           => intval( $_GET['width'] ),
								'inlineId'        => 'wi-afld-group-announcements-wrapper',
								'class'           => 'thickbox',
								'modal'           => true
							), esc_url_raw( admin_url( 'admin-ajax.php' ) ) );
							$Records[] = array(
								'DT_RowId'  => get_the_ID(),
								'id'        => get_the_ID(),
								'title'     => get_the_title(),
								'author'    => get_the_author(),
								'date'      => get_the_date(),
								'permalink' => get_the_permalink( get_the_ID() ),
								'view_url'  => $view_url,
							);
						}
						wp_reset_query();
					}
				}
				$Result['draw']            = intval( $Draw );
				$Result['recordsTotal']    = intval( $RecordsTotal );
				$Result['recordsFiltered'] = intval( $RecordsFiltered );
				$Result['data']            = $Records;
				wp_send_json( $Result );
				wp_die();
			}
		}
		
		public function wi_afld_get_announcement_callback() {
			$announcement = get_post( $_GET['announcement_id'] );
			echo $announcement->post_content;
			exit();
			die();
		}

	}
}