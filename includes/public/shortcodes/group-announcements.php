<?php

namespace WI\AFLD\Public\ShortCodes;

if ( ! class_exists( '\WI\AFLD\Public\ShortCodes\GroupAnnouncements' ) ) {
	class GroupAnnouncements {

		/**
		 * Traits used inside class
		 */
		use \WI\AFLD\Traits\Helpers;
		use \WI\AFLD\Traits\Singleton;
		use \WI\AFLD\Traits\Template;

		public function __construct() {
			add_action( 'wp_enqueue_scripts', [ $this, 'wi_afld_frontend_wp_table_styles' ], 10 );
			add_shortcode( 'wi_afld_group_announcements', array( $this, 'wi_afld_group_announcements_shortcode' ) );
		}

		public function wi_afld_frontend_wp_table_styles() {
			global $post;
			$has_shortcode = has_shortcode( $post->post_content, 'wi_afld_group_announcements' ) || has_shortcode( $post->post_content, 'wi_afld_group_announcements' );
			if ( is_a( $post, 'WP_Post' ) && $has_shortcode ) {
				wp_enqueue_style( 'list-tables-css', admin_url( 'css/list-tables.css' ), array(), time(), 'all' );
				wp_register_style( 'wi-afld-wp-table-stylesheet', WIAFLD_ASSETS_URL . '/public/css/wp-table-style.css', array(), time(), 'all' );
				wp_enqueue_style( 'wi-afld-wp-table-stylesheet' );
			}
		}

		public function wi_afld_group_announcements_shortcode( $atts ) {
			$atts = shortcode_atts( array(
				'group_id' => 0,
				'style'    => 'wp_table'
			), $atts, 'wi_afld_group_announcements' );

			return $this->get_template(
				'shortcodes.group-announcements',
				array(),
				false,
				false
			);
		}

	}
}