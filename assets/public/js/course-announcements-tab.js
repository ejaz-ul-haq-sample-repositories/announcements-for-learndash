(function ($, w) {

    $(document).ready(function ($) {

        $('.ld-tab').click(function () {
            var user_id = wi_afld_course_announcements.utilities.user_id;
            var course_id = wi_afld_course_announcements.utilities.course_id;
            var course_announcements_tab_id = 'ld-announcements-tab-' + course_id;
            var element_it = $(this).attr('id');
            if ($(this).attr('id') == course_announcements_tab_id) {
                $('.learndash-shortcode-wrap-course_content-' + course_id + '_' + course_id + '_' + user_id).addClass('afld_hidden');
            } else {
                $('.learndash-shortcode-wrap-course_content-' + course_id + '_' + course_id + '_' + user_id).removeClass('afld_hidden');
            }
        });

        var scren_width = parseInt($(window).width());
        scren_width = (scren_width - ((scren_width / 100) * 10));

        var scren_height = parseInt($(window).height());
        scren_height = (scren_height - ((scren_height / 100) * 10));

        var data = {
            course_id: wi_afld_course_announcements.utilities.course_id,
            width: scren_width,
            height: scren_height
        };

        $('#wi-afld-course-announcements-table').dataTable({
            "order": [[0, 'desc']],
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "processing": true,
            "serverSide": true,
            select: {
                style: 'multi',
                selector: 'td:first-child input[type="checkbox"]',
                className: 'row-selected'
            },
            ajax: {
                url: wi_afld_course_announcements.ajax.get.url,
                data: $.extend({
                    action: wi_afld_course_announcements.ajax.get.action,
                    security: wi_afld_course_announcements.ajax.get.nonce,
                }, data),
            },
            columns: [
                {
                    className: 'title',
                    data: 'title',
                },
                {data: 'author'},
                {data: 'date'},
                {
                    className: 'afld-actions',
                    data: 'view_url',
                    render: function (data, type) {
                        if (type === 'display') {
                            return '<button class="announcement-view-link thickbox" href="' + data + '"  data-title="' + data.title + '" class="url"><span class="dashicons dashicons-visibility"></span> <span>View</span></button> ';
                        }
                        return data;
                    }
                },
            ],
        });


        $('#wi-afld-course-announcements-table tbody').on('click', 'td button.announcement-view-link', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            var announcement_id = $(this).parents('tr').attr('id');
            var announcement_title = $(this).parent('td').siblings('.title').text();
            var popup_title_html = '<div class="afld-popup-header-wrapper">';
            popup_title_html += '<strong>Title: </strong>';
            popup_title_html += '<span>' + announcement_title + '</span>';
            popup_title_html += '</div>';
            tb_show(popup_title_html, url);
        });

    });

})(jQuery, window);