
/*
* Help: References
* https://stackoverflow.com/a/45278943/8814880
* https://awhitepixel.com/blog/wordpress-gutenberg-complete-guide-development-environment/
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
* https://webpack.js.org/concepts/entry-points/
* https://webpack.js.org/concepts/output/
* */

const defaultConfig = require("@wordpress/scripts/config/webpack.config");
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    ...defaultConfig,
    entry: {
        'admin': './src/admin/index.js',
    },
    output: {
        path: path.join(__dirname, '../assets/admin/build'),
        filename: '[name].js'
    },
};