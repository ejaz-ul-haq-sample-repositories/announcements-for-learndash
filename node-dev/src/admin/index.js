/**
 * WordPress dependencies
 */
/**
 * https://awhitepixel.com/blog/wordpress-gutenberg-create-custom-block-part-8-translation/
 */
import {__, _e, _n, sprintf,} from "@wordpress/i18n";

import {
    BaseControl,
    Button,
    PanelBody,
    PanelRow,
    Spinner,
    ToggleControl,
    Notice
} from "@wordpress/components";
import {
    render,
    Component,
    Fragment,
} from "@wordpress/element";

import apiFetch from '@wordpress/api-fetch';

import {map} from "lodash";

import {Tab} from 'bootstrap';

import './style.scss';

class Test extends Component {
    constructor() {
        super(...arguments);

        this.changeOptions = this.changeOptions.bind(this);

        this.state = {
            isLoadedSettingsAPI: false,
            isAPISaving: false,
            ChangesArePending: 0,
            ChangesAreSaved: 0,
            ChangesAreNotSaved: 0,
            wi_afld_license_key: '',
            wi_afld_license_status: false,
            wi_afld_license_admin_notice_success: 0,
            wi_afld_license_admin_notice_success_message: '',
            wi_afld_license_admin_notice_error: 0,
            wi_afld_license_admin_notice_error_message: '',
        };
    }

    componentDidMount() {
        wp.api.loadPromise.then(() => {
            this.settings = new wp.api.models.Settings();
            if (false === this.state.isLoadedSettingsAPI) {
                this.settings.fetch().then(response => {
                    this.setState({
                        wi_afld_license_key: response.wi_afld_license_key,
                        wi_afld_license_status: response.wi_afld_license_status,
                        isLoadedSettingsAPI: true
                    });
                });
            }
        });
    }

    license_activate_or_deactivate(edd_action, license_key) {
        // POST
        apiFetch({
            path: '/wi-afld/v1/license',
            method: 'POST',
            data: {
                license_key: license_key,
                license_edd_action: edd_action
            },
        }).then(res => {
            this.setState({
                isAPISaving: false,
                ChangesArePending: 0,
                ChangesAreSaved: (1 + this.state.ChangesAreSaved),
                wi_afld_license_admin_notice_success: ('success' == res.admin_notice_type) ? (1) : (0),
                wi_afld_license_admin_notice_success_message: res.admin_notice_message,
                wi_afld_license_admin_notice_error: ('error' == res.admin_notice_type) ? (1) : (0),
                wi_afld_license_admin_notice_error_message: res.admin_notice_message,
                wi_afld_license_status: ('success' == res.admin_notice_type) ? ('valid') : ('invalid'),
            });
        });
    }

    changeOptions(option, value) {
        this.setState({
            isAPISaving: true,
            ChangesArePending: 1,
        });

        const model = new wp.api.models.Settings({
            // eslint-disable-next-line camelcase
            [option]: value
        });

        model.save().then(response => {
            this.setState({
                [option]: response[option],
                isAPISaving: false,
                ChangesArePending: 0,
                ChangesAreSaved: (1 + this.state.ChangesAreSaved),
            });
        }).fail(response => {
            this.setState({
                isAPISaving: false,
                ChangesAreNotSaved: (1 + this.state.ChangesAreNotSaved),
            });
            if ('INPUT' == jQuery('*[data-state_id="' + option + '"]').prop("tagName")) {
                jQuery('*[data-state_id="' + option + '"]').addClass('settings_not_saved');
            } else if ('SELECT' == jQuery('*[data-state_id="' + option + '"]').prop("tagName")) {
                jQuery('*[data-state_id="' + option + '"]').siblings('div').addClass('settings_not_saved');
            }

        });
    }

    notices() {
        let CustomNotice;
        if (this.state.ChangesAreNotSaved > 0) {
            CustomNotice = <Notice status={'error'} isDismissible={false}>
                Failed to update settings.
            </Notice>;
        } else {
            if (this.state.ChangesArePending > 0) {
                CustomNotice = <Notice status={'warning'} isDismissible={false}>
                    <Spinner/>
                    Please wait while the changes are being saved.
                </Notice>;
            } else if (this.state.ChangesAreSaved > 0) {
                CustomNotice = <Notice status={'success'} isDismissible={false}>
                    Settings have been updated.
                </Notice>;
            }
        }

        if (this.state.wi_afld_license_admin_notice_success > 0) {
            CustomNotice = <Notice status={'success'} isDismissible={false}>
                {this.state.wi_afld_license_admin_notice_success_message}
            </Notice>;
        } else if (this.state.wi_afld_license_admin_notice_error > 0) {
            CustomNotice = <Notice status={'error'} isDismissible={false}>
                {this.state.wi_afld_license_admin_notice_error_message}
            </Notice>;
        }
        return CustomNotice;
    }

    render() {
        return (
            <Fragment>

                <div className="jumbotron jumbotron-fluid bg-white mt-1 mb-2 p-2 ">
                    <div className="container">
                        <div className={'row'}>
                            <div className={'col-md-12'}>
                                <h1 className="display-6">
                                    {__('Announcements For LearnDash', 'wi-afld')}
                                    {!this.state.isLoadedSettingsAPI || this.state.isAPISaving ? <Spinner/> : ''}
                                </h1>
                                <p className="lead">{__('This addon makes it super easy to design beautiful and important announcements and share them with your students.', 'wi-afld')}</p>
                            </div>
                            <hr/>
                            <div className={'col-md-10'}>
                                <ul className="nav nav-pills mb-1" id="pills-tab" role="tablist">
                                    <li className="nav-item" role="presentation">
                                        <button className="nav-link active mr-1" id="pills-license-tab"
                                                data-bs-toggle="pill"
                                                data-bs-target="#pills-license" type="button" role="tab"
                                                aria-controls="pills-license" aria-selected="true">
                                            <span className="dashicons dashicons-admin-network"></span>
                                            {__('License', 'wi-afld')}
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div className={'col-md-2'}>
                                <div className="button-group">
                                    <Button
                                        isPrimary
                                        disabled={this.state.isAPISaving || false == this.state.isLoadedSettingsAPI || this.state.isAPISaving}
                                        onClick={() => {

                                            this.setState({
                                                ChangesArePending: 0,
                                                ChangesAreNotSaved: 0,
                                                ChangesAreSaved: 0,
                                            });

                                            this.changeOptions('wi_afld_license_key', this.state.wi_afld_license_key);
                                        }}
                                    >
                                        {__('Save Settings', 'wi-afld')}
                                    </Button>
                                </div>
                            </div>
                            <div className={'col-md-12 mt-1'}>
                                {this.notices()}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container settings-container">
                    <div className={'row'}>
                        <div className={'col-md-8'}>
                            <div className="tab-content" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-license" role="tabpanel"
                                     aria-labelledby="pills-license-tab">
                                    <PanelBody
                                        title={__('License Settings', 'wi-afld')}
                                        initialOpen={true}
                                        icon={!this.state.isLoadedSettingsAPI || this.state.isAPISaving ?
                                            <Spinner/> : ''}
                                    >
                                        <PanelRow>
                                            <BaseControl
                                                label={__('License Key', 'wi-afld')}
                                                help={__('Please enter the license key to get future updates. The license key was emailed when you purchased this addon and also you can find it on your user dashboard screen under the subscription tab inside the invoice details.', 'wi-afld')}
                                                className="base_control"
                                            >
                                                <input
                                                    type="text"
                                                    value={this.state.wi_afld_license_key}
                                                    placeholder={__('License Key', 'wi-afld')}
                                                    disabled={this.state.isAPISaving || false == this.state.isLoadedSettingsAPI}
                                                    onChange={(e) => {
                                                        this.setState({wi_afld_license_key: e.target.value});
                                                    }}
                                                    data-state_id={'wi_afld_license_key'}
                                                />
                                            </BaseControl>
                                        </PanelRow>
                                        <PanelRow>
                                            <ToggleControl
                                                className={'license_action_btn'}
                                                label={__('Activate or Deactivate License', 'wi-afld')}
                                                help={__('You can activate and deactivate license key by using this toggle button.', 'wi-afld')}
                                                checked={'valid' == this.state.wi_afld_license_status}
                                                disabled={!this.state.wi_afld_license_key || this.state.isAPISaving || false == this.state.isLoadedSettingsAPI}
                                                onChange={() => {
                                                    let edd_action = 'activate';
                                                    if ('valid' == this.state.wi_afld_license_status) {
                                                        edd_action = 'deactivate';
                                                    }
                                                    this.setState({ChangesArePending: true});
                                                    this.license_activate_or_deactivate(edd_action, this.state.wi_afld_license_key);
                                                }}
                                            />
                                        </PanelRow>
                                    </PanelBody>
                                </div>
                            </div>
                        </div>

                        <div className={'col-md-4'}>
                            <PanelBody>
                                <div className="codeinwp-info">
                                    <h2>{__('Got a question for us?', 'wi-afld')}</h2>

                                    <p>{__('We would love to help you out if you need any help.', 'wi-afld')}</p>

                                    <div className="buttons-group">
                                        <Button
                                            target="_blank"
                                            href="https://www.wpinnovators.com/open-support-ticket/"
                                            className={'mr-1'}
                                        >
                                            <span className="mr-1 dashicons dashicons-sos"></span>
                                            {__('Support', 'wi-afld')}
                                        </Button>

                                        <Button
                                            target="_blank"
                                            href="https://www.wpinnovators.com/documentations/announcements-for-learndash/"
                                        >
                                            <span className="mr-1 dashicons dashicons-book-alt"></span>
                                            {__('Documentation', 'wi-afld')}

                                        </Button>
                                    </div>
                                </div>
                            </PanelBody>
                        </div>

                    </div>
                </div>

            </Fragment>
        );
    }

}

render(
    <Test/>,
    document.getElementById('wi-afld')
);